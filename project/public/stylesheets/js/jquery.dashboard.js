/**
* Theme: Velonic Admin Template
* Author: Coderthemes
* Module/App: Dashboard Application
*/

function scriptQuery(){ 
   var script = document.getElementsByTagName('script'); // 자신을 찾기위해 <script> 태그들을 찾습니다. 
   script = script[script.length-1].src // 가장 마지막이 자신이 됩니다 
      .replace(/^[^\?]+\?/, '') // 물음표 이전을 지우고 
     .replace(/#.+$/, '') // 혹시 모를 해쉬도 지웁니다 
      .split('&') // '&'으로 나눕니다 
   var queries = {} // 결과용 
     , query; 
    while(script.length){ // &으로 나눈 갯수만큼 
         query = script.shift().split('='); // =로 나눠 
         queries[query[0]] = decodeURIComponent(query[1],"UTF-8"); // 앞은 배열키, 뒤는 배열 값  // 앞은 배열키, 뒤는 배열 값 
   } 
   return queries; 
 } 
 var our = scriptQuery(); // 스크립트 주소에서 쿼리를 받아 저장 
 
 
var useradd = JSON.parse(our.useradd);
var noteadd = JSON.parse(our.noteadd);
var adadd = JSON.parse(our.adadd);
var advertiseradd = JSON.parse(our.advertiseradd);

var d=new Date();
var date = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();

var day ='0'+String(d.getDate());
var day2 ='0'+ String(d.getDate()-1);
var day3 ='0'+ String(d.getDate()-2);
var day4 ='0'+ String(d.getDate()-3);
var day5 ='0'+ String(d.getDate()-4);
var day6 ='0'+ String(d.getDate()-5);
var day7 ='0'+ String(d.getDate()-6);
if(d.getDate()>=10){
	day=String(d.getDate());
}
if(d.getDate()-1>=10){
	day2=String(d.getDate()-1);
}
if(d.getDate()-2>=10){
	day3=String(d.getDate()-2);
}
if(d.getDate()-3>=10){
	day4=String(d.getDate()-3);
}
if(d.getDate()-4>=10){
	day5=String(d.getDate()-4);
}
if(d.getDate()-5>=10){
	day6=String(d.getDate()-5);
}
if(d.getDate()-6>=10){
	day7=String(d.getDate()-6);
}
var adduser=0;
var adduser2=0;
var adduser3=0;
var adduser4=0;
var adduser5=0;
var adduser6=0;
var adduser7=0;
var addnote=0;
var addnote2=0;
var addnote3=0;
var addnote4=0;
var addnote5=0;
var addnote6=0;
var addnote7=0;
var addad=0;
var addad2=0;
var addad3=0;
var addad4=0;
var addad5=0;
var addad6=0;
var addad7=0;
var addadvertiser=0;
var addadvertiser2=0;
var addadvertiser3=0;
var addadvertiser4=0;
var addadvertiser5=0;
var addadvertiser6=0;
var addadvertiser7=0;
if(useradd!=null){

	for(var a=0;a<useradd.length;a++){
		if(day7==useradd[a].date){
			adduser7=useradd[a].count;
		}else if(day6==useradd[a].date){
			adduser6=useradd[a].count;
		}else if(day5==useradd[a].date){
			adduser5=useradd[a].count;
		}else if(day4==useradd[a].date){
			adduser4=useradd[a].count;
		}else if(day3==useradd[a].date){
			adduser3=useradd[a].count;
		}else if(day2==useradd[a].date){
			adduser2=useradd[a].count;
		}else if(day==useradd[a].date){
			adduser=useradd[a].count;
		}
		
	}
}
if(noteadd!=null){
	for(var a=0;a<noteadd.length;a++){
		if(day7==noteadd[a].date){
			addnote7=noteadd[a].count;
		}else if(day6==noteadd[a].date){
			addnote6=noteadd[a].count;
		}else if(day5==noteadd[a].date){
			addnote5=noteadd[a].count;
		}else if(day4==noteadd[a].date){
			addnote4=noteadd[a].count;
		}else if(day3==noteadd[a].date){
			addnote3=noteadd[a].count;
		}else if(day2==noteadd[a].date){
			addnote2=noteadd[a].count;
		}else if(day==noteadd[a].date){
			addnote=noteadd[a].count;
		}
		
	}
}
if(advertiseradd!=null){
	for(var a=0;a<advertiseradd.length;a++){
		if(day7==advertiseradd[a].date){
			addadvertiser7=advertiseradd[a].count;
		}else if(day6==advertiseradd[a].date){
			addadvertiser6=advertiseradd[a].count;
		}else if(day5==advertiseradd[a].date){
			addadvertiser5=advertiseradd[a].count;
		}else if(day4==advertiseradd[a].date){
			addadvertiser4=advertiseradd[a].count;
		}else if(day3==advertiseradd[a].date){
			addadvertiser3=advertiseradd[a].count;
		}else if(day2==advertiseradd[a].date){
			addadvertiser2=advertiseradd[a].count;
		}else if(day==advertiseradd[a].date){
			addadvertiser=advertiseradd[a].count;
		}
		
	}
}
if(adadd!=null){

	for(var a=0;a<adadd.length;a++){
		if(day7==adadd[a].date){
			addad7=adadd[a].count;
		}else if(day6==adadd[a].date){
			addad6=adadd[a].count;
		}else if(day5==adadd[a].date){
			addad5=adadd[a].count;
		}else if(day4==adadd[a].date){
			addad4=adadd[a].count;
		}else if(day3==adadd[a].date){
			addad3=adadd[a].count;
		}else if(day2==adadd[a].date){
			addad2=adadd[a].count;
		}else if(day==adadd[a].date){
			addad=adadd[a].count;
		}
		
	}
}

if(useradd!=null&noteadd!=null&adadd!=null&advertiseradd!=null){
	
	var Linechart= function(){  
		Morris.Line({
	    element: 'morris-line-example',
	    data: [
	    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-6), a: adduser7,  b: addnote7, c: addadvertiser7,d: addad7},
	    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-5), a: adduser6,  b: addnote6, c: addadvertiser6,d: addad6},
	    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-4), a: adduser5,  b: addnote5, c: addadvertiser5,d: addad5},
	    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-3), a: adduser4,  b: addnote4, c: addadvertiser4,d: addad4},
	    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-2), a: adduser3,  b: addnote3, c: addadvertiser3,d: addad3},
	    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-1), a: adduser2,  b: addnote2, c: addadvertiser2,d: addad2},
	    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()), a: adduser,  b: addnote, c: addadvertiser,d: addad}
	    
	    ],
	    xkey: 'y',
	    ykeys: ['a', 'b','c','d'],
	    labels: ['유저', '쪽지', '광고주', '광고'],
	    resize: true,
	    lineColors: ['rgba(28, 168, 221, 0.8)', 'rgba(51, 184, 108, 0.8)','rgba(241, 60, 110, 0.8)','rgba(97, 92, 168, 0.8)']
		});

		}
	}else if(useradd!=null){
		Linechart= function(){  
			Morris.Line({
		    element: 'morris-line-example',
		    data: [
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-6), a: adduser7},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-5), a: adduser6},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-4), a: adduser5},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-3), a: adduser4},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-2), a: adduser3},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-1), a: adduser2},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()), a: adduser}
		    
		    ],
		    xkey: 'y',
		    ykeys: ['a'],
		    labels: ['유저'],
		    resize: true,
		    lineColors: ['rgba(28, 168, 221, 0.8)']
			});

			}
	}else if(noteadd!=null){
		Linechart= function(){  
			Morris.Line({
		    element: 'morris-line-example',
		    data: [
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-6), a: addnote7},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-5), a: addnote6},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-4), a: addnote5},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-3), a: addnote4},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-2), a: addnote3},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-1), a: addnote2},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()), a: addnote}
		    
		    ],
		    xkey: 'y',
		    ykeys: ['a'],
		    labels: ['쪽지'],
		    resize: true,
		    lineColors: ['rgba(51, 184, 108, 0.8)']
			});

			}
	}else if(advertiseradd!=null){
		Linechart= function(){  
			Morris.Line({
		    element: 'morris-line-example',
		    data: [
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-6), a: addadvertiser7},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-5), a: addadvertiser6},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-4), a: addadvertiser5},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-3), a: addadvertiser4},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-2), a: addadvertiser3},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-1), a: addadvertiser2},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()), a: addadvertiser}
		    
		    ],
		    xkey: 'y',
		    ykeys: ['a'],
		    labels: ['광고주'],
		    resize: true,
		    lineColors: ['rgba(241, 60, 110, 0.8)']
			});

			}
	}else if(adadd!=null){
		Linechart= function(){  
			Morris.Line({
		    element: 'morris-line-example',
		    data: [
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-6), a: addad7},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-5), a: addad6},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-4), a: addad5},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-3), a: addad4},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-2), a: addad3},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()-1), a: addad2},
		    	{ y: d.getFullYear()+"-"+(d.getMonth()+1)+"-"+(d.getDate()), a: addad}
		    
		    ],
		    xkey: 'y',
		    ykeys: ['a'],
		    labels: ['광고'],
		    resize: true,
		    lineColors: ['rgba(97, 92, 168, 0.8)']
			});

			}
}






!function($) {
    "use strict";

    var Dashboard = function() {
        this.$body = $("body")
    };

    //initializing various charts and components
    Dashboard.prototype.init = function() {
        /**
        * Morris charts
        */

        //Line chart
    		Linechart();
    		
        //Chat application -> You can initialize/add chat application in any page.
        $.ChatApp.init();
    },
    //init dashboard
    $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
    
}(window.jQuery),

//initializing dashboad
function($) {
    "use strict";
    $.Dashboard.init()
}(window.jQuery);



