
/**
* Theme: Velonic Admin Template
* Author: Coderthemes
* SweetAlert - 
* Usage: $.SweetAlert.methodname
*/

!function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {
        
    //Basic
    $('#sa-basic').click(function(){
        swal("비밀번호가 다릅니다.");
    });
    $('#sa-basic2').click(function(){
        swal("Check Please.");
    });
    $('#sa-basic3').click(function(){
        swal("이미 존재하는 아이디입니다.");
    });

    //A title with a text under
    $('#sa-title').click(function(){
        swal("비밀번호를 다시 설정해주세요.", "보안을 위해 8자리 이상 만들어주세요.")
    });
    $('#sa-request').click(function(){
        swal("요청 대기중입니다.", "요청해주신 광고 계획 내용을 토대로 검토중입니다.")
    });
    $('#sa-date').click(function(){
        swal("날짜를 다시 설정해주세요.", "처음 날짜와 끝 날짜 기준을 지켜주세요.")
    });

    //Success Message
    $('#sa-success').click(function(){
        swal("회원가입에 성공하였습니다!", "짝짝짝", "success")
        return false;
    });
    $('#sa-logout').click(function(){
    	 swal("성공적으로 완료하셨습니다.", "5초 뒤 자동 로그아웃 됩니다. 다시 로그인해주세요.", "success")
         return true;
     });
    //Warning Message
    $('#sa-pictureClick').click(function(){
    	swal({   
    		title: "사업자 요청이 필요합니다.",   
    		text: "확인을 누르시면 사업자 요청 페이지로 이동합니다.",   
    		type: "warning",   
    		confirmButtonColor: "#DD6B55",   
    		confirmButtonText: "확인",   
    		closeOnConfirm: true ,
    		showCancelButton: true,   
    		cancelButtonText:"취소",
    	}, function(){   
    		location.href="/advertiserRequest";
    	});
    });
    $('#sa-limited').click(function(){
        swal({   
            title: "더 이상 만드실 수 없습니다.",   
            text: "이벤트 수량을 체크해주세요!",   
            type: "warning",   
            showCancelButton: false,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "확인",   
            closeOnConfirm: true, 
        }, function(){   
           
        });
    });
    $('#sa-info').click(function(){
        swal({   
            title: "이벤트 정보를 넣어주세요.",   
            text: "필수 정보(회사명,이벤트명,내용)를 입력해 주셔야 합니다!",   
            type: "warning",   
            showCancelButton: false,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "확인",   
            closeOnConfirm: true, 
        }, function(){   
           
        });
    });
    $('#sa-warning').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
    });

    //Parameter
    $('#sa-params').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            } 
        });
    });

    //Custom Image
    $('#sa-image').click(function(){
        swal({   
            title: "Sweet!",   
            text: "Here's a custom image.",   
            imageUrl: "assets/sweet-alert/thumbs-up.jpg" 
        });
    });

    //Auto Close Timer
    $('#sa-close').click(function(){
         swal({   
            title: "Auto close alert!",   
            text: "I will close in 2 seconds.",   
            timer: 2000,   
            showConfirmButton: false 
        });
    });


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);