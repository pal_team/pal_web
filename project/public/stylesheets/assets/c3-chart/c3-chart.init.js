/**
* Theme: Velonic Admin Template
* Author: Coderthemes
* Chart c3 page
*/



function scriptQuery(){ 
   var script = document.getElementsByTagName('script'); // 자신을 찾기위해 <script> 태그들을 찾습니다. 
   script = script[script.length-1].src // 가장 마지막이 자신이 됩니다 
      .replace(/^[^\?]+\?/, '') // 물음표 이전을 지우고 
     .replace(/#.+$/, '') // 혹시 모를 해쉬도 지웁니다 
      .split('&') // '&'으로 나눕니다 
   var queries = {} // 결과용 
     , query; 
    while(script.length){ // &으로 나눈 갯수만큼 
         query = script.shift().split('='); // =로 나눠 
         queries[query[0]] = decodeURIComponent(query[1],"UTF-8"); // 앞은 배열키, 뒤는 배열 값  // 앞은 배열키, 뒤는 배열 값 
   } 
   return queries; 
 } 
 var our = scriptQuery(); // 스크립트 주소에서 쿼리를 받아 저장 
 var useradd = JSON.parse(our.useradd);
 var advertiseradd = JSON.parse(our.advertiseradd);
 var revenue = JSON.parse(our.revenue);
 
 var d=new Date();
 var adda=0;
 var adda2=0;
 var adda3=0;
 var adda4=0;
 var adda5=0;
 var adda6=0;
 var adda7=0;
 var addb=0;
 var addb2=0;
 var addb3=0;
 var addb4=0;
 var addb5=0;
 var addb6=0;
 var addb7=0;
 var reve=0;
 var reve2=0;
 var reve3=0;
 var reve4=0;
 var reve5=0;
 var reve6=0;
 var reve7=0;
 
 for(var a=0;a<useradd.length;a++){
		if(d.getFullYear()-6==useradd[a].date){
			adda7=useradd[a].count;
		}else if(d.getFullYear()-5==useradd[a].date){
			adda6=useradd[a].count;
		}else if(d.getFullYear()-4==useradd[a].date){
			adda5=useradd[a].count;
		}else if(d.getFullYear()-3==useradd[a].date){
			adda4=useradd[a].count;
		}else if(d.getFullYear()-2==useradd[a].date){
			adda3=useradd[a].count;
		}else if(d.getFullYear()-1==useradd[a].date){
			adda2=useradd[a].count;
		}else if(d.getFullYear()==useradd[a].date){
			adda=useradd[a].count;
		}
		
	}for(var a=0;a<advertiseradd.length;a++){
		if(d.getFullYear()-6==advertiseradd[a].date){
			addb7=advertiseradd[a].count;
		}else if(d.getFullYear()-5==advertiseradd[a].date){
			addb6=advertiseradd[a].count;
		}else if(d.getFullYear()-4==advertiseradd[a].date){
			addb5=advertiseradd[a].count;
		}else if(d.getFullYear()-3==advertiseradd[a].date){
			addb4=advertiseradd[a].count;
		}else if(d.getFullYear()-2==advertiseradd[a].date){
			addb3=advertiseradd[a].count;
		}else if(d.getFullYear()-1==advertiseradd[a].date){
			addb2=advertiseradd[a].count;
		}else if(d.getFullYear()==advertiseradd[a].date){
			addb=advertiseradd[a].count;
		}
		
	}
	for(var a=0;a<revenue.length;a++){
		if(d.getFullYear()-6==revenue[a].date){
			reve7=revenue[a].sum;
		}else if(d.getFullYear()-5==revenue[a].date){
			reve6=revenue[a].sum;
		}else if(d.getFullYear()-4==revenue[a].date){
			reve5=revenue[a].sum;
		}else if(d.getFullYear()-3==revenue[a].date){
			reve4=revenue[a].sum;
		}else if(d.getFullYear()-2==revenue[a].date){
			reve3=revenue[a].sum;
		}else if(d.getFullYear()-1==revenue[a].date){
			reve2=revenue[a].sum;
		}else if(d.getFullYear()==revenue[a].date){
			reve=revenue[a].sum;
		}
		
	}

!function($) {
    "use strict";

    var ChartC3 = function() {};

    ChartC3.prototype.init = function () {
        //generating chart 
        c3.generate({
            bindto: '#chart',
            data: {
                columns: [
                    ['유저증가', adda6,adda5,adda4,adda3,adda2,adda],
                    ['사업자증가', adda6,adda5,adda4,adda3,adda2,adda]
                ],
                type: 'bar',
                colors: {
                    data1: '#dcdcdc',
                    data2: '#3bc0c3'
                },
                color: function (color, d) {
                    // d will be 'id' when called for legends
                    return d.id && d.id === 'data3' ? d3.rgb(color).darker(d.value / 150) : color;
                }
            }
        });

        //combined chart
        c3.generate({
            bindto: '#combine-chart',
            data: {
                columns: [
                    ['data1', 30, 20, 50, 40, 60, 50],
                    ['data2', 200, 130, 90, 240, 130, 220],
                    ['data3', 300, 200, 160, 400, 250, 250],
                    ['data4', 200, 130, 90, 240, 130, 220],
                    ['data5', 130, 120, 150, 140, 160, 150]
                ],
                types: {
                    data1: 'bar',
                    data2: 'bar',
                    data3: 'spline',
                    data4: 'line',
                    data5: 'bar'
                },
                colors: {
                    data1: '#dcdcdc',
                    data2: '#3bc0c3',
                    data3: '#1a2942',
                    data4: '#E67A77',
                    data5: '#95D7BB'
                },
                groups: [
                    ['data1','data2']
                ]
            },
            axis: {
                x: {
                    type: 'categorized'
                }
            }
        });
        
        //roated chart
        c3.generate({
            bindto: '#roated-chart',
            data: {
                columns: [
                ['data1', 30, 200, 100, 400, 150, 250],
                ['data2', 50, 20, 10, 40, 15, 25]
                ],
                types: {
                data1: 'bar'
                },
                colors: {
                data1: '#1a2942',
                data2: '#3bc0c3'
            },
            },
            axis: {
                rotated: true,
                x: {
                type: 'categorized'
                }
            }
        });

        //stacked chart
        c3.generate({
            bindto: '#chart-stacked',
            data: {
                columns: [
                    ['data1', reve6,reve5,reve4,reve3,reve2,reve]
                ],
                types: {
                    data1: 'area-spline'
                    // 'line', 'spline', 'step', 'area', 'area-step' are also available to stack
                },
                colors: {
                    data1: '#1a2942',
                }
            }
        });

    },
    $.ChartC3 = new ChartC3, $.ChartC3.Constructor = ChartC3

}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.ChartC3.init()
}(window.jQuery);




