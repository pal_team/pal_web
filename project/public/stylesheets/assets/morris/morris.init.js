
/**
* Theme: Velonic Admin Template
* Author: Coderthemes
* Morris Chart
*/


function scriptQuery(){ 
   var script = document.getElementsByTagName('script'); // 자신을 찾기위해 <script> 태그들을 찾습니다. 
   script = script[script.length-1].src // 가장 마지막이 자신이 됩니다 
      .replace(/^[^\?]+\?/, '') // 물음표 이전을 지우고 
     .replace(/#.+$/, '') // 혹시 모를 해쉬도 지웁니다 
      .split('&') // '&'으로 나눕니다 
   var queries = {} // 결과용 
     , query; 
    while(script.length){ // &으로 나눈 갯수만큼 
         query = script.shift().split('='); // =로 나눠 
         queries[query[0]] = decodeURIComponent(query[1],"UTF-8"); // 앞은 배열키, 뒤는 배열 값  // 앞은 배열키, 뒤는 배열 값 
   } 
   return queries; 
 } 
 var our = scriptQuery(); // 스크립트 주소에서 쿼리를 받아 저장 
 var ad = JSON.parse(our.ad);
 var event = JSON.parse(our.event);
 var out = JSON.parse(our.out);
 var topa = JSON.parse(our.top);
 alert(topa);

 
 var d=new Date();
 var adda=0;
 var adda2=0;
 var adda3=0;
 var adda4=0;
 var adda5=0;
 var adda6=0;
 var adda7=0;
 var addb=0;
 var addb2=0;
 var addb3=0;
 var addb4=0;
 var addb5=0;
 var addb6=0;
 var addb7=0;
 var addc=0;
 var addc2=0;
 var addc3=0;
 var addc4=0;
 var addc5=0;
 var addc6=0;
 var addc7=0;

 
 for(var a=0;a<ad.length;a++){
		if(d.getFullYear()-6==ad[a].date){
			adda7=ad[a].sum;
		}else if(d.getFullYear()-5==ad[a].date){
			adda6=ad[a].sum;
		}else if(d.getFullYear()-4==ad[a].date){
			adda5=ad[a].sum;
		}else if(d.getFullYear()-3==ad[a].date){
			adda4=ad[a].sum;
		}else if(d.getFullYear()-2==ad[a].date){
			adda3=ad[a].sum;
		}else if(d.getFullYear()-1==ad[a].date){
			adda2=ad[a].sum;
		}else if(d.getFullYear()==ad[a].date){
			adda=ad[a].sum;
		}
		
	} 
 for(var a=0;a<event.length;a++){
		if(d.getFullYear()-6==event[a].date){
			addb7=event[a].sum;
		}else if(d.getFullYear()-5==event[a].date){
			addb6=event[a].sum;
		}else if(d.getFullYear()-4==event[a].date){
			addb5=event[a].sum;
		}else if(d.getFullYear()-3==event[a].date){
			addb4=event[a].sum;
		}else if(d.getFullYear()-2==event[a].date){
			addb3=event[a].sum;
		}else if(d.getFullYear()-1==event[a].date){
			addb2=event[a].sum;
		}else if(d.getFullYear()==event[a].date){
			addb=event[a].sum;
		}
		
	} 
 for(var a=0;a<out.length;a++){
		if(d.getFullYear()-6==out[a].date){
			addc7=out[a].sum;
		}else if(d.getFullYear()-5==out[a].date){
			addc6=out[a].sum;
		}else if(d.getFullYear()-4==out[a].date){
			addc5=out[a].sum;
		}else if(d.getFullYear()-3==out[a].date){
			addc4=out[a].sum;
		}else if(d.getFullYear()-2==out[a].date){
			addc3=out[a].sum;
		}else if(d.getFullYear()-1==out[a].date){
			addc2=out[a].sum;
		}else if(d.getFullYear()==out[a].date){
			addc=out[a].sum;
		}
		
	}
!function($) {
    "use strict";

    var MorrisCharts = function() {};

    //creates line chart
    MorrisCharts.prototype.createLineChart = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Line({
          element: element,
          data: data,
          xkey: xkey,
          ykeys: ykeys,
          labels: labels,
          resize: true, //defaulted to true
          lineColors: lineColors
        });
    },
    //creates area chart
    MorrisCharts.prototype.createAreaChart = function(element, pointSize, lineWidth, data, xkey, ykeys, labels, lineColors) {
        Morris.Area({
            element: element,
            pointSize: 0,
            lineWidth: 0,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            resize: true,
            lineColors: lineColors
        });
    },
    //creates Bar chart
    MorrisCharts.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            barColors: lineColors
        });
    },
    //creates Donut chart
    MorrisCharts.prototype.createDonutChart = function(element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            colors: colors
        });
    },
    MorrisCharts.prototype.init = function() {

        //create line chart
        var $data  = [
            { y: String(d.getFullYear()-6), a: adda7, b: addb7, c: addc7 },
            { y: String(d.getFullYear()-5), a: adda6,  b: addb6, c: addc6 },
            { y: String(d.getFullYear()-4), a: adda5,  b: addb5, c: addc5 },
            { y: String(d.getFullYear()-3), a: adda4,  b: addb4, c: addc4 },
            { y: String(d.getFullYear()-2), a: adda3,  b: addb3, c: addc3 },
            { y: String(d.getFullYear()-1), a: adda2,  b: addb2, c: addc2 },
            { y: String(d.getFullYear()), a: adda, b: addb, c: addc }
          ];
        this.createLineChart('morris-line-example', $data, 'y', ['a', 'b','c'], ['총 증가량', '이벤트 증가량','옥외 증가량'], ['#1a2942', '#3bc0c3','#dcdcdc']);

        //creating area chart
        var $areaData = [
                { y: '2009', a: 10, b: 20 },
                { y: '2010', a: 75,  b: 65 },
                { y: '2011', a: 50,  b: 40 },
                { y: '2012', a: 75,  b: 65 },
                { y: '2013', a: 50,  b: 40 },
                { y: '2014', a: 75,  b: 65 },
                { y: '2015', a: 90, b: 60 }
            ];
        this.createAreaChart('morris-area-example', 0, 0, $areaData, 'y', ['a', 'b'], ['Series A', 'Series B'], ['#1a2942', '#3bc0c3']);

        //creating bar chart
        var $barData  = [
        	{ y: topa[0].id, a: topa[0].sum},
        	{ y: topa[1].id, a: topa[1].sum},
        	{ y: topa[2].id, a: topa[2].sum},
        	{ y: topa[3].id, a: topa[3].sum},
        	{ y: topa[4].id, a: topa[4].sum}
            
        ];
        this.createBarChart('morris-bar-example', $barData, 'y', ['a'], ['광고주 수익 순위'], ['#3bc0c3']);

        //creating donut chart
        var $donutData = [
                {label: "Download Sales", value: 12},
                {label: "In-Store Sales", value: 30},
                {label: "Mail-Order Sales", value: 20}
            ];
        this.createDonutChart('morris-donut-example', $donutData, ['#dcdcdc', '#3bc0c3', '#1a2942']);
    },
    //init
    $.MorrisCharts = new MorrisCharts, $.MorrisCharts.Constructor = MorrisCharts
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.MorrisCharts.init();
}(window.jQuery);