/**
* Theme: Velonic Admin Template
* Author: Coderthemes
* Google Maps
*/
function scriptQuery(){ 
   var script = document.getElementsByTagName('script'); // 자신을 찾기위해 <script> 태그들을 찾습니다. 
   script = script[script.length-1].src // 가장 마지막이 자신이 됩니다 
      .replace(/^[^\?]+\?/, '') // 물음표 이전을 지우고 
      .replace(/#.+$/, '') // 혹시 모를 해쉬도 지웁니다 
      .split('&') // '&'으로 나눕니다 
   var queries = {} // 결과용 
     ,query; 
    while(script.length){ // &으로 나눈 갯수만큼 
         query = script.shift().split('='); // =로 나눠 
         queries[query[0]] =decodeURIComponent(query[1],"UTF-8"); // 앞은 배열키, 뒤는 배열 값 
   } 
   return decodeURIComponent(query[1],"UTF-8"); 
 } 
var our = scriptQuery(); // 스크립트 주소에서 쿼리를 받아 저장 
var note= JSON.parse(our);

var citymap = {
		  chicago: {
		    center: {lat: 41.878, lng: -87.629},
		    population: 2714856
		  },
		  newyork: {
		    center: {lat: 40.714, lng: -74.005},
		    population: 8405837
		  },
		  losangeles: {
		    center: {lat: 34.052, lng: -118.243},
		    population: 3857799
		  },
		  vancouver: {
		    center: {lat: 49.25, lng: -123.1},
		    population: 603502
		  }
		};

!function($) {
    "use strict";
   
    var GoogleMap = function() {};
    

    //creates basic map
    GoogleMap.prototype.createBasic = function($container) {
       var map = new GMaps({
          zoom:8,
    	  div: $container,
          lat: 37.4898486,
          lng: 127.026478
        });
       for(var i=0; i<note.length;i++){
       	var a = note[i];
       map.addMarker({        	
       	lat: a.lat,
           lng: a.lng,
           title: a.title+" by "+a.id,
           infoWindow: {
             content: "<p>"+a.content+"<br/>"+a.lat+","+a.lng+"</p>"
           }
         });
       };
       return map;
    },
    
   
    //creates map with markers
    GoogleMap.prototype.createMarkers = function($container) {
 
    	var map = new GMaps({
          div: $container,
          lat: 37.4898486,
          lng: 127.026478
        });

        for(var i=0; i<note.length;i++){
        	var a = note[i];
        map.addMarker({        	
        	lat: a.lat,
            lng: a.lng,
            title: a.title+" by "+a.id,
            infoWindow: {
              content: "<p>"+a.content+"<br/>"+a.lat+","+a.lng+"</p>"
            }
          });
        };
        
        map.addMarker({
            lat: -12.043333,
            lng: -77.03,
            title: 'Lima',
            details: {
              database_id: 42,
              author: 'HPNeo'
            },
            click: function(e){
              if(console.log)
                console.log(e);
              alert('You clicked in this marker');
            }
          });

        return map;
    },
    //creates map with polygone
    GoogleMap.prototype.createWithPolygon = function ($container, $path) {
      var map = new GMaps({
        div: $container,
        lat: 37.4898486,
        lng: 127.026478
      });
     
      var polygon = map.drawPolygon({
        paths: $path,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35
      });
     
      return map;
    },

    //creates map with overlay
    GoogleMap.prototype.createWithOverlay = function ($container) {
      var map = new GMaps({
        div: $container,
        lat: -12.043333,
        lng: -77.028333
      });
      map.drawOverlay({
        lat: map.getCenter().lat(),
        lng: map.getCenter().lng(),
        content: '<div class="gmaps-overlay">Our Office!<div class="gmaps-overlay_arrow above"></div></div>',
        verticalAlign: 'top',
        horizontalAlign: 'center'
      });

      return map;
    },

    //creates map with street view
    GoogleMap.prototype.createWithStreetview = function ($container, $lat, $lng) {
      return GMaps.createPanorama({
        el: $container,
        lat : $lat,
        lng : $lng
      });
    },
    //Routes
    GoogleMap.prototype.createWithRoutes = function ($container, $lat, $lng) {
      var map = new GMaps({
        div: $container,
        lat: $lat,
        lng: $lng
      });
      $('#start_travel').click(function(e){
        e.preventDefault();
        map.travelRoute({
          origin: [-12.044012922866312, -77.02470665341184],
          destination: [-12.090814532191756, -77.02271108990476],
          travelMode: 'driving',
          step: function(e){
            $('#instructions').append('<li>'+e.instructions+'</li>');
            $('#instructions li:eq('+e.step_number+')').delay(450*e.step_number).fadeIn(200, function(){
              map.setCenter(e.end_location.lat(), e.end_location.lng());
              map.drawPolyline({
                path: e.path,
                strokeColor: '#131540',
                strokeOpacity: 0.6,
                strokeWeight: 6
              });
            });
          }
        });
      });
      return map;
    },
    //Type
    GoogleMap.prototype.createMapByType = function ($container, $lat, $lng) {
      var map = new GMaps({
        div: $container,
        lat: $lat,
        lng: $lng,
        mapTypeControlOptions: {
          mapTypeIds : ["hybrid", "roadmap", "satellite", "terrain", "osm", "cloudmade"]
        }
      });
      map.addMapType("osm", {
        getTileUrl: function(coord, zoom) {
          return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
        },
        tileSize: new google.maps.Size(256, 256),
        name: "OpenStreetMap",
        maxZoom: 18
      });
      map.addMapType("cloudmade", {
        getTileUrl: function(coord, zoom) {
          return "http://b.tile.cloudmade.com/8ee2a50541944fb9bcedded5165f09d9/1/256/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
        },
        tileSize: new google.maps.Size(256, 256),
        name: "CloudMade",
        maxZoom: 18
      });
      map.setMapTypeId("osm");
      return map;
    },
    GoogleMap.prototype.createWithMenu = function ($container, $lat, $lng) {
      var map = new GMaps({
        div: $container,
        lat: $lat,
        lng: $lng
      });
      map.setContextMenu({
        control: 'map',
        options: [{
          title: 'Add marker',
          name: 'add_marker',
          action: function(e){
            this.addMarker({
              lat: e.latLng.lat(),
              lng: e.latLng.lng(),
              title: 'New marker'
            });
            this.hideContextMenu();
          }
        }, {
          title: 'Center here',
          name: 'center_here',
          action: function(e){
            this.setCenter(e.latLng.lat(), e.latLng.lng());
          }
        }]
      });
    },
    //init
    GoogleMap.prototype.init = function() {
      var $this = this;
      $(document).ready(function(){
        //creating basic map
        $this.createBasic('#gmaps-basic'),
        //with sample markers
        $this.createMarkers('#gmaps-markers');

        //polygon
        var x=85;
        var y=-175;
        var path = [[x,y],[x,-y],[-x,y],[-x,-y]];
        $this.createWithPolygon('#gmaps-polygons', path);

        //overlay
        $this.createWithOverlay('#gmaps-overlay');

        //street view
        $this.createWithStreetview('#panorama',  42.3455, -71.0983);

        //routes
        $this.createWithRoutes('#gmaps-route',-12.043333, -77.028333);

        //types
        $this.createMapByType('#gmaps-types', -12.043333, -77.028333);

        //statu
        $this.createWithMenu('#gmaps-menu', -12.043333, -77.028333);
      });
    },
    //init
    $.GoogleMap = new GoogleMap, $.GoogleMap.Constructor = GoogleMap
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.GoogleMap.init()
}(window.jQuery);