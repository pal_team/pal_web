/*
 * 설정
 */
module.exports = {
    server_port: 9000,
    db_info: {
        connectionLimit: 100,
        host: 'pal.cjunlwuslzir.ap-northeast-2.rds.amazonaws.com',
        user: 'admin',
        password: 'palarm8080',
        database: 'pal',
        debug: false
    },
    route_info: [
        {file: './index', path: '/', method: 'index', type: 'get'},
        {file: './adindex', path: '/adindex', method: 'adindex', type: 'get'},
        {file: './register', path: '/register/choice', method: 'choice', type: 'get'},
        {file: './register', path: '/user', method: 'user', type: 'get'},
        {file: './register', path: '/advertiser', method: 'advertiser', type: 'get'},
        {file: './register', path: '/userRegister', method: 'userRegister', type: 'post'},
        {file: './register', path: '/advertiserRegister', method: 'advertiserRegister', type: 'post'},
        {file: './register', path: '/terms', method: 'terms', type: 'get'},

        {file: './register', path: '/idCheck', method: 'idCheck', type: 'post'},
        {file: './register', path: '/pwCheck', method: 'pwCheck', type: 'post'},

        {file: './login', path: '/login', method: 'login', type: 'get'},
        {file: './login', path: '/recoverpw', method: 'recoverpw', type: 'get'},
        {file: './login', path: '/register', method: 'register', type: 'get'},
        {file: './login', path: '/authUser', method: 'authUser', type: 'post'},
        {file: './login', path: '/logout', method: 'logout', type: 'get'},
        {file: './login', path: '/three', method: 'three', type: 'get'},

        {file: './note', path: '/note/list', method: 'list', type: 'get'},
        {file: './note', path: '/note/insertForm', method: 'insertForm', type: 'get'},
        {file: './note', path: '/note/insert', method: 'insert', type: 'post'},

        {file: './comment', path: '/comment/list/:id,:date', method: 'list', type: 'get'},
        {file: './comment', path: '/comment/insertForm/:id,:date', method: 'insertForm', type: 'get'},
        {file: './comment', path: '/comment/insert', method: 'insert', type: 'post'},

        {file: './admin', path: '/adminPage', method: 'adminPage', type: 'get'},
        {file: './admin', path: '/statisticsAll', method: 'statisticsAll', type: 'get'},
        {file: './admin', path: '/statisticsNote', method: 'statisticsNote', type: 'get'},
        {file: './admin', path: '/statisticsAdvertiser', method: 'statisticsAdvertiser', type: 'get'},
        {file: './admin', path: '/customerFacing', method: 'customerFacing', type: 'get'},

        {file: './admin', path: '/findMember', method: 'findMember', type: 'get'},
        {file: './admin', path: '/findMessage', method: 'findMessage', type: 'get'},
        
        {file: './admin', path: '/mailing', method: 'mailing', type: 'get'},
        {file: './admin', path: '/notice', method: 'notice', type: 'get'},

        {file:'./admin', path:'/infoUser', method:'infoUser', type:'get'},
		{file:'./admin', path:'/gradeAdvertiser', method:'gradeAdvertiser', type:'get'},
		{file:'./admin', path:'/updateAdvertiser', method:'updateAdvertiser', type:'post'},
		{file:'./admin', path:'/updateAdvertiser2/:id', method:'updateAdvertiser2', type:'get'},
		{file:'./admin', path:'/datepicker', method:'datepicker', type:'post'},
		
		
        {file: './responseJSON', path: '/searchNote', method: 'searchNote', type: 'post'},
        {file: './responseJSON', path: '/createNote', method: 'createNote', type: 'post'},
        {file: './responseJSON', path: '/mauthUser', method: 'mauthUser', type: 'post'},
        {file: './responseJSON', path: '/getfollow', method: 'getfollow', type: 'post'},
        {file: './responseJSON', path: '/getfollower', method: 'getfollower', type: 'post'},
        {file: './responseJSON', path: '/getmynote', method: 'getmynote', type: 'post'},
        {file: './responseJSON', path: '/logout', method: 'logout', type: 'post'},
        {file: './responseJSON', path: '/addfollow', method: 'addfollow', type: 'post'},
        {file: './responseJSON', path: '/removefollow', method: 'removefollow', type: 'post'},

        {file: './advertiser', path: '/advertiserPage', method: 'advertiserPage', type: 'get'},
        
        {file: './advertiser', path: '/advertiserStatisticsAll', method: 'advertiserStatisticsAll', type: 'get'},
        {file: './advertiser', path: '/advertiserStatisticsNote', method: 'advertiserStatisticsNote', type: 'get'},
        {file: './advertiser', path: '/advertiserStatisticsAdvertiser', method: 'advertiserStatisticsAdvertiser', type: 'get'},
        
        {file: './advertiser', path: '/advertiserRequest', method: 'advertiserRequest', type: 'get'},
        {file: './advertiser', path: '/requestRegistration', method: 'requestRegistration', type: 'post'},
        {file: './advertiser', path: '/adRegistration', method: 'adRegistration', type: 'get'},
        {file: './advertiser', path: '/adRegistrationPost', method: 'adRegistrationPost', type: 'post'},

        {file:'./admin', path: '/goCs', method: 'goCs', type: 'get'},
        {file:'./admin', path:'/cs', method:'cs', type:'get'},
		{file:'./admin', path:'/updateCs', method:'updateCs', type:'post'},
		{file:'./admin', path:'/updateCs2/:id', method:'updateCs2', type:'get'},
		{file:'./advertiser', path:'/faq', method:'faq', type:'get'},
        {file:'./admin', path:'/goFaq', method:'goFaq', type:'get'},
        {file:'./advertiser', path:'/goQna', method:'goQna', type:'get'},
        {file:'./admin', path:'/writeFaq', method:'writeFaq', type:'post'},
        {file:'./advertiser', path:'/writeQna', method:'writeQna', type:'post'},
        
        {file: './advertiser', path: '/adinfo/:id,:created_date', method: 'adinfo', type: 'get'},
        {file: './advertiser', path: '/adcomment', method: 'adcomment', type: 'post'},
        {file: './advertiser', path: '/myadlist', method: 'myadlist', type: 'get'},
        {file: './advertiser', path: '/adupdate', method: 'adupdate', type: 'post'},
    	{file:'./admin', path:'/noteexception/:id,:created_date', method:'noteexception', type:'get'},
		{file:'./admin', path:'/adexception/:id,:created_date', method:'adexception', type:'get'},
		{file:'./admin', path:'/userexception/:id', method:'userexception', type:'get'},
		{file:'./admin', path:'/advertiserexception/:id', method:'advertiserexception', type:'get'},
		{file:'./admin', path:'/repeatexception/:id,:content,:title', method:'repeatexception', type:'get'},
		{file:'./admin', path: '/sanction', method: 'sanction', type: 'get'},
        {file:'./admin', path: '/sanctionexception', method: 'sanctionexception', type: 'get'}
    ]
};