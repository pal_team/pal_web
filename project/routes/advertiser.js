﻿var multer = require('multer');
var fs=require('fs');
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, 'D:/Lecture/pal/pal_web/project/public/a');
    },
    filename: function (req, file, callback) {
        callback(null, Date.now()+file.originalname);
    }
});

var upload = multer({
    storage: storage,
    limits: {
        files: 100,
        fileSize: 1024 * 1024 *1024
    }
}).array('photo', 1);


var advertiserPage = function(req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }

        var note;
        var noteadd;
        var adadd;
        var advertiseradd;
        var notecnt;
        var ad;
        var adcnt;
        var advertiser;
        var userall;

        var noteaddsql = 'SELECT count(DATE_FORMAT(created_date, "%Y-%m-%d")) count ,DATE_FORMAT(created_date, "%d") date FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%d") order by date;';
        var adaddsql = 'SELECT count(DATE_FORMAT(created_date, "%Y-%m-%d")) count ,DATE_FORMAT(created_date, "%d") date FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%d") order by date;';
        var advertiseraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%d") date FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%d") order by date;';
        var notecntsql = 'SELECT sum(view_cnt) view_cnt,DATE_FORMAT(created_date, "%Y") date FROM pal_note where  DATE_FORMAT(created_date, "%Y")   between year(subdate(now(), INTERVAL 3 YEAR)) and year(now())  group by DATE_FORMAT(created_date, "%Y")  ;';
        var adcntsql = 'SELECT sum(view_cnt) view_cnt,DATE_FORMAT(created_date, "%Y") date FROM pal_ad where  DATE_FORMAT(created_date, "%Y")   between year(subdate(now(), INTERVAL 3 YEAR)) and year(now())  group by DATE_FORMAT(created_date, "%Y")  ;';
        var notesql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP ;';
        var adsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP ;';
        var advertisersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP ;';
        var usersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP ;';


        conn.query(notesql, function(err, rows) {
            conn.release();
            note = rows[0].maxnumber;
        });conn.query(noteaddsql, function(err, rows) {
            console.log("rows : " + JSON.stringify(rows));
            noteadd = rows;
        });conn.query(adaddsql, function(err, rows) {
            console.log("rows : " + JSON.stringify(rows));
            adadd = rows;
        });conn.query(advertiseraddsql, function(err, rows) {
            console.log("rows : " + JSON.stringify(rows));
            advertiseradd = rows;
        });conn.query(notecntsql, function(err, rows) {
            notecnt = rows;
        });conn.query(adcntsql, function(err, rows) {
            adcnt = rows;
        });conn.query(adsql, function(err, rows) {
            ad = rows[0].maxnumber;
        });   conn.query(advertisersql, function(err, rows) {
            advertiser = rows[0].maxnumber;
        });
        conn.query(usersql, function(err, rows) {
            userall = rows[0].maxnumber;
            res.render("advertiser/advertiserPage",{note:note,noteadd:noteadd,adadd:adadd,advertiseradd:advertiseradd,notecnt:notecnt,ad:ad,adcnt:adcnt,advertiser:advertiser,userall:userall,user:req.session.user});
        });




    });
};

var advertiserStatisticsAll = function(req, res){
    var pool = req.app.get('database');
    var useradd;
    var advertiseradd;
    var revenue;
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }
        var advertiseraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%Y") date FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%Y") order by date;';
        var useraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%Y") date FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%Y") order by date;';
        var   revenuesql= 'SELECT sum(cost) sum,DATE_FORMAT(created_date, "%Y") date FROM pal_ad_cost where  date(created_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%Y") order by date;';

        conn.query(advertiseraddsql, function(err, rows) {
            conn.release();
            advertiseradd = rows;
        });
        conn.query(useraddsql, function(err, rows) {
            useradd = rows;
        });
        conn.query(revenuesql, function(err, rows) {
            revenue = rows;
            res.render("advertiser/advertiserStatistics/advertiserStatisticsAll",{useradd:useradd,advertiseradd:advertiseradd,revenue:revenue,user:req.session.user});
        });
    });
};


var advertiserStatisticsNote = function(req,res){
    var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
        var sql = "SELECT A.* from(select (@rownum:=@rownum+1) row,pal_note.* from pal_note order by view_cnt desc)A,(select @rownum:=0)R";
        var sql2 = "SELECT location_name, count(*) AS Best_location FROM pal_note GROUP BY location_name ORDER BY Best_location DESC";
        connection.query(sql2, function (err, result) {
            if (err) console.error("err : " + err);
            Best_location = result;
        });
        connection.query(sql, function (err, rows) {

            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
            res.render('advertiser/advertiserStatistics/advertiserStatisticsNote', {rows:rows,user:req.session.user,Best_location:Best_location});
        });


    });
};



var advertiserStatisticsAdvertiser = function(req, res){
    var pool = req.app.get('database');
    var ad;
    var event;
    var out;
    var top;
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }
        var adrevenuesql = 'SELECT sum(view_cnt) sum,DATE_FORMAT(created_date, "%Y") date FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%Y") order by date;';
        var eventnenuesql = 'SELECT sum(view_cnt) sum,DATE_FORMAT(a.created_date, "%Y") date, ad_type event FROM pal_ad a,pal_ad_event e where a.created_date=e.created_date and date(a.created_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(a.created_date) <= date(now()) group by DATE_FORMAT(a.created_date, "%Y") order by date;';
        var   outrevenuesql= 'SELECT sum(view_cnt) sum,DATE_FORMAT(a.created_date, "%Y") date, ad_type event FROM pal_ad a,pal_ad_outdoors o where a.created_date=o.created_date and date(a.created_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(a.created_date) <= date(now()) group by DATE_FORMAT(a.created_date, "%Y") order by date;';
        var   topsql= 'SELECT sum(view_cnt) sum,id FROM pal_ad group by id order by sum desc limit 0 , 5;';

        conn.query(adrevenuesql, function(err, rows) {
            conn.release();
            ad = rows;
        });
        conn.query(eventnenuesql, function(err, rows) {
            event = rows;
        });
        conn.query(outrevenuesql, function(err, rows) {
            out = rows;
        });
        conn.query(topsql, function(err, rows) {
            top = rows;
            res.render("advertiser/advertiserStatistics/advertiserStatisticsAdvertiser",{ad:ad,event:event,out:out,top:top,user:req.session.user});
        });
    });

};


var advertiserRequest = function(req, res){
    res.render("advertiser/ad/advertiserRequest",{user:req.session.user});
};


var requestRegistration = function(req,res){
    var pool = req.app.get('database');
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }

        var id = req.body.id;
        var company_name = req.body.company_name;
        var ad_name = req.body.ad_name;
        var company_address = req.body.company_address;
        var detail_plan = req.body.detail_plan;

        var data = {
            id : id,
            company_name : company_name,
            ad_name : ad_name,
            company_address : company_address,
            detail_plan : detail_plan
        };
        var sql = 'insert into pal_ad_request set ?';
        var sql2 = "update pal_advertiser set grade='R' where id = '"+id+"'";


        conn.query(sql,data, function (err, result) {
            conn.release();
<<<<<<< HEAD
            if (err) console.error("err : " + err);
        });
        conn.query(sql2, function (err, result) {
            if (err) console.error("err : " + err);
        });
=======
            if (err) console.error("err : " + err);
        });
        conn.query(sql2, function (err, result) {
            if (err) console.error("err : " + err);
        });
>>>>>>> c818fdc39dd64519f36143d99ad2d5309d432c62
        setTimeout(function(){
            res.redirect('/logout');
        },1000*5);



    });
};

var adRegistration = function(req, res){
    res.render("advertiser/ad/adRegistration",{user:req.session.user});
};


var writeQna = function(req, res) {
<<<<<<< HEAD
<<<<<<< HEAD
		   var upFile;
		   var fileOrigin;
		   upload(req,res,function(err) {


		   var files = req.files; //첨부파일 갯수가 1개든 2개든 모두 array로 인식된다!
		   for (var i = 0 ; i < files.length; i++) {
		        var originalFileNm = files[i].originalname;
		        var savedFileNm = files[i].filename;
		        var fileSize = files[i].size;
		        /*test*/console.log("originalFileNm : '%s', savedFileNm : '%s', size : '%s'",  originalFileNm, savedFileNm, fileSize);
		   
		   }
		    upFile=savedFileNm;
		    if(err) {
		        return res.end("Error uploading file." );
		    }
		   });
		var pool = req.app.get('database');
		pool.getConnection(function(err, conn) {
			if (err) {
				conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
				return;
			}
			var id = req.body.id;
			var title = req.body.title;		
			var category = req.body.category;
			var content = req.body.content;
		
			
			console.log(id);
			
			
			var data = {
					id : id,
					title : title,			
					category : category,
					content : content,
					file : upFile
								
					
			};
			var que = 'insert into pal_qna set ?';
			var exec = conn.query(que, data, function(err, result) {
				conn.release();
				console.log('실행 대상 SQL : ' + exec.sql);
				if (err) {
					console.log('SQL 실행 시 에러 발생함.');
					console.dir(err);
					
					
				}
				
				res.redirect("/advertiserPage");
				
			});
		});
		
	};

	var goQna = function(req, res) {
		res.render('advertiser/adQna/writeQna',{user:req.session.user});
	};

	var faq = function(req,res){
		var pool = req.app.get('database');
	    pool.getConnection(function (err, conn) {
	    	if (err) {
				conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
				return;
			}
	    	
	    	var que = 'select *from pal_faq;';
			conn.query(que, function(err, rows) {
				conn.release();
				
				res.render('advertiser/adQna/faq',{rows:rows, user:req.session.user});
	   
	        });
	     
	    });
	};   


	var checkFaq = function (req, res) {
		var pool = req.app.get('database');
		pool.getConnection(function (err, connection){
			
			var List = "select * from pal_faq;";
			
			connection.query(List, function (err, rows){
				if(err) console.error("err : " + err);
				console.log("rows : " + JSON.stringify(rows));
				connection.release();
			});
			
			res.render('advertiser/advertiserMypage/writeFaq',{title: 'faq 조회', rows:rows});
		});
	};

	
	var myadlist = function(req, res) {
		   var pool = req.app.get('database');
		    pool.getConnection(function (err, connection) {
		       var id = req.session.user.id;
		     console.log(id);
		       myadlist = "SELECT DATE_FORMAT(created_date, '%Y-%m-%d  %H:%i:%s') created_date,DATE_FORMAT(start_date, '%Y-%m-%d  %H:%i:%s') start_date,DATE_FORMAT(end_date, '%Y-%m-%d  %H:%i:%s') end_date,id,company_name,title,content,media,design,ad_type,location_name,lat,lng,view_cnt,report,private from pal_ad where id = '"+id+"'";
		        
		       connection.query(myadlist, function (err, rows) {
		            if (err) console.error("err : " + err);
		            console.log("result : " + JSON.stringify(rows));
		            connection.release();
		           res.render("advertiser/advertiserMypage/profile",{user:req.session.user,rows:rows})
	        });
	    });
	};
	var adinfo = function(req, res) {
		   var pool = req.app.get('database');
		    pool.getConnection(function (err, connection) {
			      var id = req.params.id;
			      var created_date = req.params.created_date;
		     console.log(id);
		       var myadlist = "SELECT id,DATE_FORMAT(created_date, '%Y-%m-%d %H:%i:%s') created_date,company_name,DATE_FORMAT(start_date, '%Y-%m-%d %H:%i:%s') start_date,DATE_FORMAT(end_date, '%Y-%m-%d %H:%i:%s') end_date,title,content,media,ad_type,location_name,view_cnt from pal_ad where id = '"+id+"' and created_date = '"+created_date+"'";
		        
		       connection.query(myadlist, function (err, rows) {

					console.log('실행 대상 SQL : ' + myadlist);
		            if (err) console.error("err : " + err);
		            console.log("result : " + JSON.stringify(rows));
		            connection.release();
		           res.render("advertiser/advertiserMypage/profile",{user:req.session.user,rows:rows})
		       	});
		    });
	};
	var adupdate = function(req, res) {
		   var pool = req.app.get('database');
		    pool.getConnection(function (err, connection) {
		    	if (err) {
		    		connection.release(); 
					return;
				}

				var id = req.body.id;
				var created_date = req.body.created_date;
				var company_name = req.body.company_name;
				var title=req.body.title;
				var content = req.body.content;
				var adupdate = "update pal_ad set company_name = '"+company_name+"' , title = '"+title+"' , content = '"+content+"' where id = '"+id+"' and created_date = '"+created_date+"'";
				connection.query(adupdate, function(err, result) {
					connection.release();
					console.log('실행 대상 SQL1 : ' + adupdate);
					if (err) {
						console.log('SQL 실행 시 에러 발생함.');
						console.dir(err);
					}
					res.redirect("/adinfo/"+id+","+created_date);
					
				});
	});
	};

	var adcomment = function(req, res) {
		   var pool = req.app.get('database');
		   var id = req.body.id;
		   var created_date = req.body.created_date;
		    pool.getConnection(function (err, connection) {
		    	if (err) {
		    		connection.release(); 
				}

				var adupdate = "select DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') date,content from pal_ad_comment where to_id = '"+id+"' and created_date = '"+created_date+"' order by date desc limit 0, 10";
				connection.query(adupdate, function(err, result) {
					connection.release();
					console.log('실행 대상 SQL2 : ' + adupdate);
					if (err) {
						console.log('SQL 실행 시 에러 발생함.');
						console.dir(err);
					}
					res.send(result);
					res.end();
					
				});
	});
	};
<<<<<<< HEAD
var usercount = function(req, res) {
	   var pool = req.app.get('database');
	    pool.getConnection(function (err, conn) {
	    	if (err) {
	    		conn.release(); 
			}
	    	var noteall;
		      var adall;
		      var advertiserall;
		      var userall;
		      var newnote;
		      var newad;
		      var newadvertiser;
		      var newuser;
		      var useradd;
		      var advertiserweek;
		      var advertisermonth;
		      var advertiseryear;
		      var adweek;
		      var admonth;
		      var adyear;
		      var userweek;
		      var usermonth;
		      var useryear;
		      var noteweek;
		      var notemonth;
		      var noteyear;
			
			
		      var useraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%d") date FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%d") order by date;';
			  var noteallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP ;';
		      var adallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP ;';
		      var advertiserallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP ;';
		      var userallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP ;';
		      var newnotesql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP where date(created_date)= date(now());';
			  var newadsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP where date(created_date)= date(now());';
			  var newadvertisersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP where date(join_date)= date(now());';
			  var newusersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP where date(join_date)=date(now()) ;';
			
		   
		      	var advertiseryearsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
		    	var advertisermonthsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
		    	var advertiserweeksql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
		    	var adyearsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
		    	var admonthsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
		    	var adweeksql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
		    	var useryearsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
		    	var usermonthsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
		    	var userweeksql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
		    	var noteyearsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
		    	var notemonthsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
		    	var noteweeksql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
		    	
		    	conn.query(advertiseryearsql, function(err, rows) {
            		advertiseryear=rows[0].count;
	       	   });conn.query(advertisermonthsql, function(err, rows) {
	       		advertisermonth=rows[0].count;
	       	   });conn.query(advertiserweeksql, function(err, rows) {
	       		  advertiserweek=rows[0].count;
	     	   });conn.query(adyearsql, function(err, rows) {
            		adyear=rows[0].count;
	       	   });conn.query(admonthsql, function(err, rows) {
	       		admonth=rows[0].count;
	       	   });conn.query(adweeksql, function(err, rows) {
	       		  adweek=rows[0].count;
	     	   });conn.query(useryearsql, function(err, rows) {
            		useryear=rows[0].count;
	       	   });conn.query(usermonthsql, function(err, rows) {
	       		usermonth=rows[0].count;
	       	   });conn.query(userweeksql, function(err, rows) {
	       		  userweek=rows[0].count;
	     	   });conn.query(noteyearsql, function(err, rows) {
            		noteyear=rows[0].count;
	       	   });conn.query(notemonthsql, function(err, rows) {
	       		notemonth=rows[0].count;
	       	   });conn.query(noteweeksql, function(err, rows) {
	       		  noteweek=rows[0].count;
	     	   });conn.query(noteallsql, function(err, rows) {
                  noteall = rows[0].maxnumber;
               });conn.query(adallsql, function(err, rows) {
                  adall = rows[0].maxnumber;
               }); conn.query(userallsql, function(err, rows) {
	               userall = rows[0].maxnumber;
	           });conn.query(advertiserallsql, function(err, rows) {
                  advertiserall = rows[0].maxnumber;
               });conn.query(newnotesql, function(err, rows) {
            	   newnote = rows[0].maxnumber;
               });conn.query(newadsql, function(err, rows) {
            	   newad = rows[0].maxnumber;
               }); conn.query(newusersql, function(err, rows) {
            	   newuser = rows[0].maxnumber;
	           });conn.query(newadvertisersql, function(err, rows) {
                  newadvertiser = rows[0].maxnumber;
               });conn.query(useraddsql, function(err, rows) {
	               useradd = rows;
	               res.render("admin/adminPage",{advertiseryear:advertiseryear,advertisermonth:advertisermonth,advertiserweek:advertiserweek,adyear:adyear,admonth:admonth,adweek:adweek,useryear:useryear,usermonth:usermonth,userweek:userweek,noteyear:noteyear,notemonth:notemonth,noteweek:noteweek,useradd:useradd,newnote:newnote,newad:newad,newadvertiser:newadvertiser,newuser:newuser,noteall:noteall,adall:adall,advertiserall:advertiserall,userall:userall,user:req.session.user});
               });
						
});
};
var advertisercount = function(req, res) {
	   var pool = req.app.get('database');
	    pool.getConnection(function (err, conn) {
	    	if (err) {
	    		conn.release(); 
			}

	    	 var noteall;
		      var adall;
		      var advertiserall;
		      var userall;
		      var newnote;
		      var newad;
		      var newadvertiser;
		      var newuser;
		      var advertiseradd;
		      var advertiserweek;
		      var advertisermonth;
		      var advertiseryear;
		      var adweek;
		      var admonth;
		      var adyear;
		      var userweek;
		      var usermonth;
		      var useryear;
		      var noteweek;
		      var notemonth;
		      var noteyear;
		      
		      
		      var advertiseraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%d") date FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%d") order by date;';
		      var noteallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP ;';
		      var adallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP ;';
		      var advertiserallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP ;';
		      var userallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP ;';
		      var newnotesql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP where date(created_date)= date(now());';
			  var newadsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP where date(created_date)= date(now());';
			  var newadvertisersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP where date(join_date)= date(now());';
			  var newusersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP where date(join_date)=date(now()) ;';
			
		   
		      	var advertiseryearsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
		    	var advertisermonthsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
		    	var advertiserweeksql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
		    	var adyearsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
		    	var admonthsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
		    	var adweeksql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
		    	var useryearsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
		    	var usermonthsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
		    	var userweeksql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
		    	var noteyearsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
		    	var notemonthsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
		    	var noteweeksql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
		    	conn.query(advertiseryearsql, function(err, rows) {
            		advertiseryear=rows[0].count;
	       	   });conn.query(advertisermonthsql, function(err, rows) {
	       		advertisermonth=rows[0].count;
	       	   });conn.query(advertiserweeksql, function(err, rows) {
	       		  advertiserweek=rows[0].count;
	     	   });conn.query(adyearsql, function(err, rows) {
            		adyear=rows[0].count;
	       	   });conn.query(admonthsql, function(err, rows) {
	       		admonth=rows[0].count;
	       	   });conn.query(adweeksql, function(err, rows) {
	       		  adweek=rows[0].count;
	     	   });conn.query(useryearsql, function(err, rows) {
            		useryear=rows[0].count;
	       	   });conn.query(usermonthsql, function(err, rows) {
	       		usermonth=rows[0].count;
	       	   });conn.query(userweeksql, function(err, rows) {
	       		  userweek=rows[0].count;
	     	   });conn.query(noteyearsql, function(err, rows) {
            		noteyear=rows[0].count;
	       	   });conn.query(notemonthsql, function(err, rows) {
	       		notemonth=rows[0].count;
	       	   });conn.query(noteweeksql, function(err, rows) {
	       		  noteweek=rows[0].count;
	     	   });conn.query(noteallsql, function(err, rows) {
                  noteall = rows[0].maxnumber;
               });conn.query(adallsql, function(err, rows) {
                  adall = rows[0].maxnumber;
               }); conn.query(userallsql, function(err, rows) {
	               userall = rows[0].maxnumber;
	           });conn.query(advertiserallsql, function(err, rows) {
                  advertiserall = rows[0].maxnumber;
               });conn.query(newnotesql, function(err, rows) {
            	   newnote = rows[0].maxnumber;
               });conn.query(newadsql, function(err, rows) {
            	   newad = rows[0].maxnumber;
               }); conn.query(newusersql, function(err, rows) {
            	   newuser = rows[0].maxnumber;
	           });conn.query(newadvertisersql, function(err, rows) {
                  newadvertiser = rows[0].maxnumber;
               });conn.query(advertiseraddsql, function(err, rows) {
	               advertiseradd = rows;
	               res.render("admin/adminPage",{advertiseryear:advertiseryear,advertisermonth:advertisermonth,advertiserweek:advertiserweek,adyear:adyear,admonth:admonth,adweek:adweek,useryear:useryear,usermonth:usermonth,userweek:userweek,noteyear:noteyear,notemonth:notemonth,noteweek:noteweek,advertiseradd:advertiseradd,newnote:newnote,newad:newad,newadvertiser:newadvertiser,newuser:newuser,noteall:noteall,adall:adall,advertiserall:advertiserall,userall:userall,user:req.session.user});
               });
});
};
var adcount = function(req, res) {
	   var pool = req.app.get('database');
	    pool.getConnection(function (err, conn) {
	    	if (err) {
	    		conn.release(); 
			}
	    	  var noteall;
		      var adall;
		      var advertiserall;
		      var userall;
		      var newnote;
		      var newad;
		      var newadvertiser;
		      var newuser;
		      var adadd;
		      var advertiserweek;
		      var advertisermonth;
		      var advertiseryear;
		      var adweek;
		      var admonth;
		      var adyear;
		      var userweek;
		      var usermonth;
		      var useryear;
		      var noteweek;
		      var notemonth;
		      var noteyear;
		      
		      
		      var que = 'select * from ?? where id=? and password =?';
		      var adaddsql = 'SELECT count(DATE_FORMAT(created_date, "%Y-%m-%d")) count ,DATE_FORMAT(created_date, "%d") date FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%d") order by date;';
		      var noteallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP ;';
		      var adallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP ;';
		      var advertiserallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP ;';
		      var userallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP ;';
		      var newnotesql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP where date(created_date)= date(now());';
			  var newadsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP where date(created_date)= date(now());';
			  var newadvertisersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP where date(join_date)= date(now());';
			  var newusersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP where date(join_date)=date(now()) ;';
			
		   
		      	var advertiseryearsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
		    	var advertisermonthsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
		    	var advertiserweeksql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
		    	var adyearsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
		    	var admonthsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
		    	var adweeksql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
		    	var useryearsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
		    	var usermonthsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
		    	var userweeksql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
		    	var noteyearsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
		    	var notemonthsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
		    	var noteweeksql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
		    	
		    	conn.query(advertiseryearsql, function(err, rows) {
            		advertiseryear=rows[0].count;
	       	   });conn.query(advertisermonthsql, function(err, rows) {
	       		advertisermonth=rows[0].count;
	       	   });conn.query(advertiserweeksql, function(err, rows) {
	       		  advertiserweek=rows[0].count;
	     	   });conn.query(adyearsql, function(err, rows) {
            		adyear=rows[0].count;
	       	   });conn.query(admonthsql, function(err, rows) {
	       		admonth=rows[0].count;
	       	   });conn.query(adweeksql, function(err, rows) {
	       		  adweek=rows[0].count;
	     	   });conn.query(useryearsql, function(err, rows) {
            		useryear=rows[0].count;
	       	   });conn.query(usermonthsql, function(err, rows) {
	       		usermonth=rows[0].count;
	       	   });conn.query(userweeksql, function(err, rows) {
	       		  userweek=rows[0].count;
	     	   });conn.query(noteyearsql, function(err, rows) {
            		noteyear=rows[0].count;
	       	   });conn.query(notemonthsql, function(err, rows) {
	       		notemonth=rows[0].count;
	       	   });conn.query(noteweeksql, function(err, rows) {
	       		  noteweek=rows[0].count;
	     	   });conn.query(noteallsql, function(err, rows) {
                  noteall = rows[0].maxnumber;
               });conn.query(adallsql, function(err, rows) {
                  adall = rows[0].maxnumber;
               }); conn.query(userallsql, function(err, rows) {
	               userall = rows[0].maxnumber;
	           });conn.query(advertiserallsql, function(err, rows) {
                  advertiserall = rows[0].maxnumber;
               });conn.query(newnotesql, function(err, rows) {
            	   newnote = rows[0].maxnumber;
               });conn.query(newadsql, function(err, rows) {
            	   newad = rows[0].maxnumber;
               }); conn.query(newusersql, function(err, rows) {
            	   newuser = rows[0].maxnumber;
	           });conn.query(newadvertisersql, function(err, rows) {
                  newadvertiser = rows[0].maxnumber;
               });conn.query(adaddsql, function(err, rows) {
	               adadd = rows;
	               res.render("admin/adminPage",{advertiseryear:advertiseryear,advertisermonth:advertisermonth,advertiserweek:advertiserweek,adyear:adyear,admonth:admonth,adweek:adweek,useryear:useryear,usermonth:usermonth,userweek:userweek,noteyear:noteyear,notemonth:notemonth,noteweek:noteweek,adadd:adadd,newnote:newnote,newad:newad,newadvertiser:newadvertiser,newuser:newuser,noteall:noteall,adall:adall,advertiserall:advertiserall,userall:userall,user:req.session.user});
               });
});
};
var notecount = function(req, res) {
	   var pool = req.app.get('database');
	    pool.getConnection(function (err, conn) {
	    	if (err) {
	    		conn.release(); 
			}
	    	 var noteall;
		      var adall;
		      var advertiserall;
		      var userall;
		      var newnote;
		      var newad;
		      var newadvertiser;
		      var newuser;
		      var noteadd;
		      var advertiserweek;
		      var advertisermonth;
		      var advertiseryear;
		      var adweek;
		      var admonth;
		      var adyear;
		      var userweek;
		      var usermonth;
		      var useryear;
		      var noteweek;
		      var notemonth;
		      var noteyear;
		      
		      
		      var noteaddsql = 'SELECT count(DATE_FORMAT(created_date, "%Y-%m-%d")) count ,DATE_FORMAT(created_date, "%d") date FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%d") order by date;';
		      var noteallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP ;';
		      var adallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP ;';
		      var advertiserallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP ;';
		      var userallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP ;';
		      var newnotesql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP where date(created_date)= date(now());';
			  var newadsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP where date(created_date)= date(now());';
			  var newadvertisersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP where date(join_date)= date(now());';
			  var newusersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP where date(join_date)=date(now()) ;';
			
		   
		      	var advertiseryearsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
		    	var advertisermonthsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
		    	var advertiserweeksql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
		    	var adyearsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
		    	var admonthsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
		    	var adweeksql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
		    	var useryearsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
		    	var usermonthsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
		    	var userweeksql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
		    	var noteyearsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
		    	var notemonthsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
		    	var noteweeksql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
		    	
	    	conn.query(advertiseryearsql, function(err, rows) {
        		advertiseryear=rows[0].count;
       	   });conn.query(advertisermonthsql, function(err, rows) {
       		advertisermonth=rows[0].count;
       	   });conn.query(advertiserweeksql, function(err, rows) {
       		  advertiserweek=rows[0].count;
     	   });conn.query(adyearsql, function(err, rows) {
        		adyear=rows[0].count;
       	   });conn.query(admonthsql, function(err, rows) {
       		admonth=rows[0].count;
       	   });conn.query(adweeksql, function(err, rows) {
       		  adweek=rows[0].count;
     	   });conn.query(useryearsql, function(err, rows) {
        		useryear=rows[0].count;
       	   });conn.query(usermonthsql, function(err, rows) {
       		usermonth=rows[0].count;
       	   });conn.query(userweeksql, function(err, rows) {
       		  userweek=rows[0].count;
     	   });conn.query(noteyearsql, function(err, rows) {
        		noteyear=rows[0].count;
       	   });conn.query(notemonthsql, function(err, rows) {
       		notemonth=rows[0].count;
       	   });conn.query(noteweeksql, function(err, rows) {
       		  noteweek=rows[0].count;
     	   });conn.query(noteallsql, function(err, rows) {
              noteall = rows[0].maxnumber;
           });conn.query(adallsql, function(err, rows) {
              adall = rows[0].maxnumber;
           }); conn.query(userallsql, function(err, rows) {
               userall = rows[0].maxnumber;
           });conn.query(advertiserallsql, function(err, rows) {
              advertiserall = rows[0].maxnumber;
           });conn.query(newnotesql, function(err, rows) {
        	   newnote = rows[0].maxnumber;
           });conn.query(newadsql, function(err, rows) {
        	   newad = rows[0].maxnumber;
           }); conn.query(newusersql, function(err, rows) {
        	   newuser = rows[0].maxnumber;
           });conn.query(newadvertisersql, function(err, rows) {
              newadvertiser = rows[0].maxnumber;
           });conn.query(noteaddsql, function(err, rows) {
               noteadd = rows;
               res.render("admin/adminPage",{advertiseryear:advertiseryear,advertisermonth:advertisermonth,advertiserweek:advertiserweek,adyear:adyear,admonth:admonth,adweek:adweek,useryear:useryear,usermonth:usermonth,userweek:userweek,noteyear:noteyear,notemonth:notemonth,noteweek:noteweek,noteadd:noteadd,newnote:newnote,newad:newad,newadvertiser:newadvertiser,newuser:newuser,noteall:noteall,adall:adall,advertiserall:advertiserall,userall:userall,user:req.session.user});
           });
=======
	var getGeocode = function(latlng){
	    var request = require('sync-request');
	    var address;
	    var response = request('get','https://maps.googleapis.com/maps/api/geocode/json',{
	        'qs': {
	            'latlng': latlng,
	            'language' : 'ko',
	            'key' : 'AIzaSyCnm2VlyenmWo3sB8tJ93sBJMfrIftWmn4'
	        }
	    });

	    if(response.statusCode === 200){
	        var json = JSON.parse(response.getBody('utf8'));
	        address = json.results[1].formatted_address;
	        return address;
	    }
	    return "Convert Geocode Error";

	};
=======
=======
>>>>>>> c818fdc39dd64519f36143d99ad2d5309d432c62
    var upFile;
    var fileOrigin;
    upload(req,res,function(err) {


        var files = req.files; //첨부파일 갯수가 1개든 2개든 모두 array로 인식된다!
        for (var i = 0 ; i < files.length; i++) {
            var originalFileNm = files[i].originalname;
            var savedFileNm = files[i].filename;
            var fileSize = files[i].size;
			/*test*/console.log("originalFileNm : '%s', savedFileNm : '%s', size : '%s'",  originalFileNm, savedFileNm, fileSize);

        }
        upFile=savedFileNm;
        if(err) {
            return res.end("Error uploading file." );
        }
    });
    var pool = req.app.get('database');
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }
        var id = req.body.id;
        var title = req.body.title;
        var category = req.body.category;
        var content = req.body.content;


        console.log(id);


        var data = {
            id : id,
            title : title,
            category : category,
            content : content,
            file : upFile


        };
        var que = 'insert into pal_qna set ?';
        var exec = conn.query(que, data, function(err, result) {
            conn.release();
            console.log('실행 대상 SQL : ' + exec.sql);
            if (err) {
                console.log('SQL 실행 시 에러 발생함.');
                console.dir(err);


            }

            res.redirect("/advertiserPage");

        });
    });

};

var goQna = function(req, res) {
    res.render('advertiser/advertiserMypage/writeQna',{user:req.session.user});
};

var faq = function(req,res){
    var pool = req.app.get('database');
    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }

        var title = req.body.title;
        var content = req.body.content;

        var que = 'select title,content from pal_faq;';
        conn.query(que, function(err, rows) {
            conn.release();

            res.render('advertiser/advertiserMypage/faq',{rows:rows, user:req.session.user});

        });

    });
};


var checkFaq = function (req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function (err, connection){

        var List = "select * from pal_faq;";

        connection.query(List, function (err, rows){
            if(err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
        });

        res.render('advertiser/advertiserMypage/writeFaq',{title: 'faq 조회', rows:rows});
    });
};


var myadlist = function(req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
        var id = req.session.user.id;
        console.log(id);
        myadlist = "SELECT DATE_FORMAT(created_date, '%Y-%m-%d  %H:%i:%s') created_date,DATE_FORMAT(start_date, '%Y-%m-%d  %H:%i:%s') start_date,DATE_FORMAT(end_date, '%Y-%m-%d  %H:%i:%s') end_date,id,company_name,title,content,media,design,ad_type,location_name,lat,lng,view_cnt,report,private from pal_ad where id = '"+id+"'";
>>>>>>> 60152f0e0b5b30c25d1b2ce3ea0211c4a239af37

        connection.query(myadlist, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("result : " + JSON.stringify(rows));
            connection.release();
            res.render("advertiser/advertiserMypage/profile",{user:req.session.user,rows:rows})
        });
    });
};
var adinfo = function(req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
        var id = req.params.id;
        var created_date = req.params.created_date;
        console.log(id);
        var myadlist = "SELECT id,DATE_FORMAT(created_date, '%Y-%m-%d %H:%i:%s') created_date,company_name,DATE_FORMAT(start_date, '%Y-%m-%d %H:%i:%s') start_date,DATE_FORMAT(end_date, '%Y-%m-%d %H:%i:%s') end_date,title,content,media,ad_type,location_name,view_cnt from pal_ad where id = '"+id+"' and created_date = '"+created_date+"'";

        connection.query(myadlist, function (err, rows) {

            console.log('실행 대상 SQL : ' + myadlist);
            if (err) console.error("err : " + err);
            console.log("result : " + JSON.stringify(rows));
            connection.release();
            res.render("advertiser/advertiserMypage/profile",{user:req.session.user,rows:rows})
        });
    });
};
var adupdate = function(req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
        if (err) {
            connection.release();
            return;
        }

        var id = req.body.id;
        var created_date = req.body.created_date;
        var company_name = req.body.company_name;
        var title=req.body.title;
        var content = req.body.content;
        var adupdate = "update pal_ad set company_name = '"+company_name+"' , title = '"+title+"' , content = '"+content+"' where id = '"+id+"' and created_date = '"+created_date+"'";
        connection.query(adupdate, function(err, result) {
            connection.release();
            console.log('실행 대상 SQL1 : ' + adupdate);
            if (err) {
                console.log('SQL 실행 시 에러 발생함.');
                console.dir(err);
            }
            res.redirect("/adinfo/"+id+","+created_date);

        });
    });
};

var adcomment = function(req, res) {
    var pool = req.app.get('database');
    var id = req.body.id;
    var created_date = req.body.created_date;
    pool.getConnection(function (err, connection) {
        if (err) {
            connection.release();
        }

        var adupdate = "select DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') date,content from pal_ad_comment where to_id = '"+id+"' and created_date = '"+created_date+"' order by date desc limit 0, 10";
        connection.query(adupdate, function(err, result) {
            connection.release();
            console.log('실행 대상 SQL2 : ' + adupdate);
            if (err) {
                console.log('SQL 실행 시 에러 발생함.');
                console.dir(err);
            }
            res.send(result);
            res.end();

        });
    });
};
var getGeocode = function(latlng){
    var request = require('sync-request');
    var address;
    var response = request('get','https://maps.googleapis.com/maps/api/geocode/json',{
        'qs': {
            'latlng': latlng,
            'language' : 'ko',
            'key' : 'AIzaSyCnm2VlyenmWo3sB8tJ93sBJMfrIftWmn4'
        }
    });

    if(response.statusCode === 200){
        var json = JSON.parse(response.getBody('utf8'));
        address = json.results[1].formatted_address;
        return address;
    }
    return "Convert Geocode Error";

};
<<<<<<< HEAD
>>>>>>> c818fdc39dd64519f36143d99ad2d5309d432c62
=======
>>>>>>> c818fdc39dd64519f36143d99ad2d5309d432c62
var adRegistrationPost = function(req, res){
    var upMedia;
    var savedFileNm;
    upload(req,res,function(err) {
        var files = req.files; //첨부파일 갯수가 1개든 2개든 모두 array로 인식된다!
        console.log(files.length +" : files.length")
        for (var i = 0 ; i < files.length; i++) {
            var originalFileNm = files[i].originalname;
            savedFileNm = files[i].filename;
            var fileSize = files[i].size;
            console.log("originalFileNm : '%s', savedFileNm : '%s', size : '%s'",  originalFileNm, savedFileNm, fileSize);
        }
        upMedia=savedFileNm;
        if(err) {
            return res.end("Error uploading file." );
        }
        var pool = req.app.get('database');
        pool.getConnection(function(err, conn) {
            if (err) {
                conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
                return;
            }
<<<<<<< HEAD
<<<<<<< HEAD
            upMedia=savedFileNm;
	    if(err) {
	        return res.end("Error uploading file." );
	    }
		var pool = req.app.get('database');
		pool.getConnection(function(err, conn) {
			if (err) {
				conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
				return;
			}
			var amount = req.body.amount;
			var id = req.body.id;
			var company_name = req.body.company_name;
			var title = req.body.title;		
			var content = req.body.content;
			var ad_type = req.body.ad_type;
			var latStr = [];
			var lngStr = [];
			var lat = [];
			var lng = [];
			latStr = req.body.lat;
		    lngStr = req.body.lng;
		    lat = parseFloat(latStr);
		    lng = parseFloat(lngStr);
			for(var i=0;i<amount;i++){
		    var latlng = latStr[i]+","+lngStr[i];
		    var location_name = getGeocode(latlng);
		    var data = {
		    		media : upMedia,
					id : id,
					company_name : company_name,
					title : title,			
					content : content,
					ad_type : ad_type,
					lat : latStr[i],
					lng : lngStr[i],
					location_name : location_name
			};
			var que = 'insert into pal_ad set ?';
			var exec = conn.query(que, data, function(err, result) {
				console.log('실행 대상 SQL : ' + exec.sql);
				if (err) {
					console.log('SQL 실행 시 에러 발생함.');
					console.dir(err);
				}
		     });
		  }res.redirect("/advertiserPage");
		});
	   });
	
=======
=======
>>>>>>> c818fdc39dd64519f36143d99ad2d5309d432c62
            var amount = req.body.amount;
            var id = req.body.id;
            var company_name = req.body.company_name;
            var title = req.body.title;
            var content = req.body.content;
            var ad_type = req.body.ad_type;
            var latStr = [];
            var lngStr = [];
            var lat = [];
            var lng = [];
            latStr = req.body.lat;
            lngStr = req.body.lng;
            lat = parseFloat(latStr);
            lng = parseFloat(lngStr);
            for(var i=0;i<amount;i++){
                var latlng = latStr[i]+","+lngStr[i];
                var location_name = getGeocode(latlng);
                var data = {
                    media : upMedia,
                    id : id,
                    company_name : company_name,
                    title : title,
                    content : content,
                    ad_type : ad_type,
                    lat : latStr[i],
                    lng : lngStr[i],
                    location_name : location_name
                };
                var que = 'insert into pal_ad set ?';
                var exec = conn.query(que, data, function(err, result) {
                    console.log('실행 대상 SQL : ' + exec.sql);
                    if (err) {
                        console.log('SQL 실행 시 에러 발생함.');
                        console.dir(err);
                    }
                });
            }res.redirect("/advertiserPage");
        });
    });

<<<<<<< HEAD
>>>>>>> c818fdc39dd64519f36143d99ad2d5309d432c62
=======
>>>>>>> c818fdc39dd64519f36143d99ad2d5309d432c62
};

module.exports.advertiserPage = advertiserPage;
module.exports.advertiserStatisticsAll = advertiserStatisticsAll;
module.exports.advertiserStatisticsNote = advertiserStatisticsNote;
module.exports.advertiserStatisticsAdvertiser = advertiserStatisticsAdvertiser;
module.exports.advertiserRequest = advertiserRequest;
module.exports.requestRegistration = requestRegistration;
module.exports.adRegistration = adRegistration;
module.exports.adRegistrationPost = adRegistrationPost;

module.exports.faq = faq;
module.exports.writeQna = writeQna;
module.exports.goQna = goQna;


module.exports.myadlist = myadlist;
module.exports.adinfo = adinfo;
module.exports.adupdate = adupdate;
module.exports.adcomment = adcomment;