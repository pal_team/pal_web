﻿var login = function (req, res) {
    console.log('/ 로그인페이지 패스 요청');

    res.render('login/login');

};

var recoverpw = function (req, res) {
    console.log('/ 비밀번호 찾기 요청');
    res.render('login/recoverpw', null);

};

var register = function (req, res) {

    console.log('/ 회원가입 페이지 요청');

    res.render('login/register', null);
};


var authUser = function(req, res) {
	   var pool = req.app.get('database');
	   pool.getConnection(function(err, conn) {
	      if (err) {
	         conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
	         return;
	      }

	      var id = req.body.id;
	      var password = req.body.password;

	      
	      var columns = ['id','password'];
	      var tablename = 'pal_advertiser';

	      var noteall;
	      var adall;
	      var advertiserall;
	      var userall;
	      var newnote;
	      var newad;
	      var newadvertiser;
	      var newuser;
	      var noteadd;
	      var adadd;
	      var advertiseradd;
	      var useradd;
	      var advertiserweek;
	      var advertisermonth;
	      var advertiseryear;
	      var adweek;
	      var admonth;
	      var adyear;
	      var userweek;
	      var usermonth;
	      var useryear;
	      var noteweek;
	      var notemonth;
	      var noteyear;
	      
	      
	      var que = 'select * from ?? where id=? and password =?';
	      var useraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%d") date FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%d") order by date;';
		  var noteaddsql = 'SELECT count(DATE_FORMAT(created_date, "%Y-%m-%d")) count ,DATE_FORMAT(created_date, "%d") date FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%d") order by date;';
	      var adaddsql = 'SELECT count(DATE_FORMAT(created_date, "%Y-%m-%d")) count ,DATE_FORMAT(created_date, "%d") date FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%d") order by date;';
	      var advertiseraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%d") date FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%d") order by date;';
	      var noteallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP ;';
	      var adallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP ;';
	      var advertiserallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP ;';
	      var userallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP ;';
	      var newnotesql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP where date(created_date)= date(now());';
		  var newadsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP where date(created_date)= date(now());';
		  var newadvertisersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP where date(join_date)= date(now());';
		  var newusersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP where date(join_date)=date(now()) ;';
		
	   
	      	var advertiseryearsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
	    	var advertisermonthsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
	    	var advertiserweeksql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
	    	var adyearsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
	    	var admonthsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
	    	var adweeksql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
	    	var useryearsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
	    	var usermonthsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
	    	var userweeksql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
	    	var noteyearsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
	    	var notemonthsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
	    	var noteweeksql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
	    	
	      conn.query(que, [tablename,id, password], function(err, rows) {
	         conn.release();
	         
	         if(rows.length > 0){
	            req.session.user = rows[0];
	            
	            console.log('로그인 완료!.');
	         
	            if(req.session.user.grade =='D'){
	            	conn.query(advertiseryearsql, function(err, rows) {
	            		advertiseryear=rows[0].count;
		       	   });conn.query(advertisermonthsql, function(err, rows) {
		       		advertisermonth=rows[0].count;
		       	   });conn.query(advertiserweeksql, function(err, rows) {
		       		  advertiserweek=rows[0].count;
		     	   });conn.query(adyearsql, function(err, rows) {
	            		adyear=rows[0].count;
		       	   });conn.query(admonthsql, function(err, rows) {
		       		admonth=rows[0].count;
		       	   });conn.query(adweeksql, function(err, rows) {
		       		  adweek=rows[0].count;
		     	   });conn.query(useryearsql, function(err, rows) {
	            		useryear=rows[0].count;
		       	   });conn.query(usermonthsql, function(err, rows) {
		       		usermonth=rows[0].count;
		       	   });conn.query(userweeksql, function(err, rows) {
		       		  userweek=rows[0].count;
		     	   });conn.query(noteyearsql, function(err, rows) {
	            		noteyear=rows[0].count;
		       	   });conn.query(notemonthsql, function(err, rows) {
		       		notemonth=rows[0].count;
		       	   });conn.query(noteweeksql, function(err, rows) {
		       		  noteweek=rows[0].count;
		     	   });conn.query(noteallsql, function(err, rows) {
	                  noteall = rows[0].maxnumber;
	               });conn.query(adallsql, function(err, rows) {
	                  adall = rows[0].maxnumber;
	               }); conn.query(userallsql, function(err, rows) {
		               userall = rows[0].maxnumber;
		           });conn.query(advertiserallsql, function(err, rows) {
	                  advertiserall = rows[0].maxnumber;
	               });conn.query(newnotesql, function(err, rows) {
	            	   newnote = rows[0].maxnumber;
	               });conn.query(newadsql, function(err, rows) {
	            	   newad = rows[0].maxnumber;
	               }); conn.query(newusersql, function(err, rows) {
	            	   newuser = rows[0].maxnumber;
		           });conn.query(newadvertisersql, function(err, rows) {
	                  newadvertiser = rows[0].maxnumber;
	               });conn.query(useraddsql, function(err, rows) {
						useradd = rows;
				   });conn.query(noteaddsql, function(err, rows) {
		                noteadd = rows;
		           });conn.query(adaddsql, function(err, rows) {
	                  adadd = rows;
	               });conn.query(advertiseraddsql, function(err, rows) {
		               advertiseradd = rows;
		               res.render("admin/adminPage",{advertiseryear:advertiseryear,advertisermonth:advertisermonth,advertiserweek:advertiserweek,adyear:adyear,admonth:admonth,adweek:adweek,useryear:useryear,usermonth:usermonth,userweek:userweek,noteyear:noteyear,notemonth:notemonth,noteweek:noteweek,noteadd:noteadd,useradd:useradd,adadd:adadd,advertiseradd:advertiseradd,newnote:newnote,newad:newad,newadvertiser:newadvertiser,newuser:newuser,noteall:noteall,adall:adall,advertiserall:advertiserall,userall:userall,user:req.session.user});
	               });
	              
	            }
	            else
	            res.render("adindex",{user:req.session.user});
	            
	         }else
	            {
	            
	            console.log("황용이형님이 실패하심ㅠㅠ");
	            res.render("login/login");
	            }
	      });
	   });
	};

var logout = function (req, res) {
    if (req.session.user) {
        console.log('로그아웃 합니다.');

        req.session.destroy(function (err) {
            if (err) {
                throw err;
            }
        });
        console.log('세션삭제 후 로그아웃 완료');
        res.redirect('/adindex');

    } else {
        console.log('로그인되지않은 상태입니다.');

        res.redirect('/adindex');
    }
};

var three = function (req, res) {

    console.log('/ 비밀번호 찾기 페이지 요청');

    res.render('login/three', null);
};


module.exports.logout = logout;
module.exports.login = login;
module.exports.recoverpw = recoverpw;
module.exports.register = register;
module.exports.authUser = authUser;
module.exports.three = three;

