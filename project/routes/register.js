var fs=require('fs');

var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, 'D:/Lecture/pal/pal_web/project/public/a')
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname + Date.now())
    }
});

var upload = multer({ 
    storage: storage,
    limits: {
		files: 10,
		fileSize: 1024 * 1024 * 1024
	}
}).array('photo', 1);


var choice = function(req, res) {

	res.render("register/choice");
	
};

var user = function(req, res) {

	res.render("register/user");
	
};

var advertiser = function(req, res) {
	
	res.render("register/advertiser");
	
};

// user에서 가입 눌렀을때 일로
var userRegister = function(req, res) {
	var pool = req.app.get('database');
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
			return;
		}

		var id = req.body.id;
		var password = req.body.password;
		var first_name = req.body.first_name;
		var last_name = req.body.last_name;
		var email = req.body.email;
		var locale = req.body.locale;
		var nickname = req.body.nickname;
		var gender = req.body.gender;

		var data = {
			id : id,
			password : password,
			first_name : first_name,
			last_name : last_name,
			email : email,
			locale : locale,
			nickname : nickname,
			gender : gender
			
		};
		var que = 'insert into pal_user set ?';
		var exec = conn.query(que, data, function(err, result) {
			conn.release();
			console.log('실행 대상 SQL : ' + exec.sql);
			if (err) {
				console.log('SQL 실행 시 에러 발생함.');
				console.dir(err);
				res.render("register/user");
				return;
			}
			setTimeout(function(){
				res.redirect("/");
				res.end;
			},1000*2);
			
		});
	});

};


var advertiserRegister =function(req, res) {
	var picture;
	var pictureorigin;
	upload(req,res,function(err) {


	var files = req.files; //첨부파일 갯수가 1개든 2개든 모두 array로 인식된다!
	for (var i = 0 ; i < files.length; i++) {
        var originalFileNm = files[i].originalname;
        var savedFileNm = files[i].filename;
        var fileSize = files[i].size;
        /*test*/console.log("originalFileNm : '%s', savedFileNm : '%s', size : '%s'",  originalFileNm, savedFileNm, fileSize);
    picture=files[i].filename;
    pictureorigin=files[i].originalname;
	}
	  
    if(err) {
        return res.end("Error uploading file." );
    }
	});
    var pool = req.app.get('database');
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
			return;
		}

		var id = req.body.id;
		var password = req.body.password;
		var first_name = req.body.first_name;
		var last_name = req.body.last_name;
		var email = req.body.email;
		var number = req.body.number;
		var address = req.body.address;
		var license = req.body.license;

		var data = {
			id : id,
			password : password,
			first_name : first_name,
			last_name : last_name,
			email : email,
			number : number,
			address : address,
			license : license,
			picture : picture,
			pictureorigin : pictureorigin
		};
		var que = 'insert into pal_advertiser set ?';
		var exec = conn.query(que, data, function(err, result) {
			conn.release();
			console.log('실행 대상 SQL : ' + exec.sql);
			if (err) {
				console.log('SQL 실행 시 에러 발생함.');
				console.dir(err);
				
				res.render("register/advertiser");
				
				return;
			}
			
			setTimeout(function(){
				res.render("adindex");
				res.end;
			},1000*3);
		});
	});
        
       

	 

};




var idCheck = function(req, res) {
	var pool = req.app.get('database');
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
			return;
		}
		var id = req.body.id;
		console.log(id);
		var que = 'select id from pal_advertiser where id=?';
		var exec = conn.query(que,id, function(err, rows) {
			conn.release();
			if(rows.length > 0){
				res.write("Already exist");
				console.log("already exist");
				
				res.end();
			}else
				{
				res.write("You can use this ID !");
				console.log("You can use this ID !");
				res.end();
				}
		});
	});	
};

var pwCheck = function(req, res) {

	
		var password = req.body.password;
		var password2 = req.body.password2;
		console.log(password);
	
			if(password !== password2 ){
				res.write("Incorrect");
				console.log("Incorrect");
				
				res.end();
			}else
				{
				res.write("Correct !");
				console.log("Correct !");
				res.end();
				}
	
	
};

var terms = function(req, res) {

	res.render("register/terms");
	
};

module.exports.userRegister = userRegister;
module.exports.advertiserRegister = advertiserRegister;
module.exports.user  = user;
module.exports.advertiser  = advertiser;
module.exports.choice = choice;
module.exports.terms = terms;
module.exports.idCheck = idCheck;
module.exports.pwCheck = pwCheck;