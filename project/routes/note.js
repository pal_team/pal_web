
// 글 목록으로 갔을시
var list = function(req,res){
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
   	// 보여줄 페이지  sql문
        var sqlForSelectList = "SELECT id,title,likes,content ,DATE_FORMAT(created_date, '%Y-%m-%d  %h:%i:%s') date FROM pal_note ;";
     // 저장되어있는 글개수 sql문
      
        
        connection.query(sqlForSelectList, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
            res.render('note/note', {title: ' 게시판 전체 글 조회',rows:rows});
        });
        
        
    });
};

// 글쓰기를 눌렀을시 html로 바로 보내줌
var insertForm = function(req,res){
    res.render("note/noteForm");
};

// 글작성후 전송을 눌렀을때
var insert = function(req,res){
	var pool = req.app.get('database');
	pool.getConnection(function(err, conn) {
        if (err) {
        	conn.release();  // 반드시 해제해야 합니다.
          return;
        }   

        var id=req.body.id;
        var title=req.body.title;
        var media=req.body.media;
    	var content=req.body.content;
    	var private=req.body.private;
    	if(private=='on'){
    		private=1;
    	}else{
    		private=0;
    	}
    	console.log(private);
    

    	// 데이터를 객체로 만듭니다.
    	var data = {id:id,title:title, content:content,media:media, private:private};
    	var a= 'insert into pal_note set ?';
        var exec = conn.query(a, data, function(err, result) {
        	conn.release(); 
        	console.log('실행 대상 SQL : ' + exec.sql);
        	if (err) {
        		console.log('SQL 실행 시 에러 발생함.');
        		console.dir(err);
        		return;
        	}
        	 res.redirect("/note/list");
        });
	});
	
};


module.exports.insert = insert;
module.exports.insertForm = insertForm;
module.exports.list = list;


