﻿var adminPage = function(req, res) {
	var pool = req.app.get('database');
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); 
			return;
		}

		  var noteall;
	      var adall;
	      var advertiserall;
	      var userall;
	      var newnote;
	      var newad;
	      var newadvertiser;
	      var newuser;
	      var noteadd;
	      var adadd;
	      var advertiseradd;
	      var useradd;
	      var advertiserweek;
	      var advertisermonth;
	      var advertiseryear;
	      var adweek;
	      var admonth;
	      var adyear;
	      var userweek;
	      var usermonth;
	      var useryear;
	      var noteweek;
	      var notemonth;
	      var noteyear;
	      
	      
	      var que = 'select * from ?? where id=? and password =?';
	      var useraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%d") date FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%d") order by date;';
		  var noteaddsql = 'SELECT count(DATE_FORMAT(created_date, "%Y-%m-%d")) count ,DATE_FORMAT(created_date, "%d") date FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%d") order by date;';
	      var adaddsql = 'SELECT count(DATE_FORMAT(created_date, "%Y-%m-%d")) count ,DATE_FORMAT(created_date, "%d") date FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%d") order by date;';
	      var advertiseraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%d") date FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 DAY)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%d") order by date;';
	      var noteallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP ;';
	      var adallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP ;';
	      var advertiserallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP ;';
	      var userallsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP ;';
	      var newnotesql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_note,(select @rownum:=0) TMP where date(created_date)= date(now());';
		  var newadsql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_ad,(select @rownum:=0) TMP where date(created_date)= date(now());';
		  var newadvertisersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_advertiser,(select @rownum:=0) TMP where date(join_date)= date(now());';
		  var newusersql = 'SELECT max(@rownum:=@rownum+1) maxnumber FROM pal_user,(select @rownum:=0) TMP where date(join_date)=date(now()) ;';
		
	   
	      	var advertiseryearsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
	    	var advertisermonthsql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
	    	var advertiserweeksql = "SELECT count(join_date) count   FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
	    	var adyearsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
	    	var admonthsql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
	    	var adweeksql = "SELECT count(created_date) count   FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
	    	var useryearsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(join_date) <= date(now()) ";
	    	var usermonthsql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(join_date) <= date(now()) ";
	    	var userweeksql = "SELECT count(join_date) count   FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 day)) and date(join_date) <= date(now()) ";
	    	var noteyearsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 YEAR)) and date(created_date) <= date(now()) ";
	    	var notemonthsql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 1 Month)) and date(created_date) <= date(now()) ";
	    	var noteweeksql = "SELECT count(created_date) count   FROM pal_note where  date(created_date) >= date(subdate(now(), INTERVAL 6 day)) and date(created_date) <= date(now()) ";
	    	
	    	conn.query(advertiseryearsql, function(err, rows) {
        		advertiseryear=rows[0].count;
       	   });conn.query(advertisermonthsql, function(err, rows) {
       		advertisermonth=rows[0].count;
       	   });conn.query(advertiserweeksql, function(err, rows) {
       		  advertiserweek=rows[0].count;
     	   });conn.query(adyearsql, function(err, rows) {
        		adyear=rows[0].count;
       	   });conn.query(admonthsql, function(err, rows) {
       		admonth=rows[0].count;
       	   });conn.query(adweeksql, function(err, rows) {
       		  adweek=rows[0].count;
     	   });conn.query(useryearsql, function(err, rows) {
        		useryear=rows[0].count;
       	   });conn.query(usermonthsql, function(err, rows) {
       		usermonth=rows[0].count;
       	   });conn.query(userweeksql, function(err, rows) {
       		  userweek=rows[0].count;
     	   });conn.query(noteyearsql, function(err, rows) {
        		noteyear=rows[0].count;
       	   });conn.query(notemonthsql, function(err, rows) {
       		notemonth=rows[0].count;
       	   });conn.query(noteweeksql, function(err, rows) {
       		  noteweek=rows[0].count;
     	   });conn.query(noteallsql, function(err, rows) {
              noteall = rows[0].maxnumber;
           });conn.query(adallsql, function(err, rows) {
              adall = rows[0].maxnumber;
           }); conn.query(userallsql, function(err, rows) {
               userall = rows[0].maxnumber;
           });conn.query(advertiserallsql, function(err, rows) {
              advertiserall = rows[0].maxnumber;
           });conn.query(newnotesql, function(err, rows) {
        	   newnote = rows[0].maxnumber;
           });conn.query(newadsql, function(err, rows) {
        	   newad = rows[0].maxnumber;
           }); conn.query(newusersql, function(err, rows) {
        	   newuser = rows[0].maxnumber;
           });conn.query(newadvertisersql, function(err, rows) {
              newadvertiser = rows[0].maxnumber;
           });conn.query(useraddsql, function(err, rows) {
				useradd = rows;
		   });conn.query(noteaddsql, function(err, rows) {
                noteadd = rows;
           });conn.query(adaddsql, function(err, rows) {
              adadd = rows;
           });conn.query(advertiseraddsql, function(err, rows) {
               advertiseradd = rows;
               res.render("admin/adminPage",{advertiseryear:advertiseryear,advertisermonth:advertisermonth,advertiserweek:advertiserweek,adyear:adyear,admonth:admonth,adweek:adweek,useryear:useryear,usermonth:usermonth,userweek:userweek,noteyear:noteyear,notemonth:notemonth,noteweek:noteweek,noteadd:noteadd,useradd:useradd,adadd:adadd,advertiseradd:advertiseradd,newnote:newnote,newad:newad,newadvertiser:newadvertiser,newuser:newuser,noteall:noteall,adall:adall,advertiserall:advertiserall,userall:userall,user:req.session.user});
           });
					
				
	
	
	
	});
};


var statisticsAll = function(req, res){
	var pool = req.app.get('database');
	var useradd;
	var advertiseradd;
	var revenue;
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
			return;
		}
	var advertiseraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%Y") date FROM pal_advertiser where  date(join_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%Y") order by date;';
	var useraddsql = 'SELECT count(DATE_FORMAT(join_date, "%Y-%m-%d")) count ,DATE_FORMAT(join_date, "%Y") date FROM pal_user where  date(join_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(join_date) <= date(now()) group by DATE_FORMAT(join_date, "%Y") order by date;';
	var	revenuesql= 'SELECT sum(cost) sum,DATE_FORMAT(created_date, "%Y") date FROM pal_ad_cost where  date(created_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%Y") order by date;';
	
	conn.query(advertiseraddsql, function(err, rows) {
		conn.release();
		advertiseradd = rows;
		});
	conn.query(useraddsql, function(err, rows) {
		useradd = rows;
		});
	conn.query(revenuesql, function(err, rows) {
		revenue = rows;
		res.render("admin/statistics/statisticsAll",{useradd:useradd,advertiseradd:advertiseradd,revenue:revenue,user:req.session.user});
		});
	});
};


var statisticsNote = function(req,res){
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
    	var Best_location;
        var sql = "SELECT A.* from(select (@rownum:=@rownum+1) row,pal_note.* from pal_note order by view_cnt desc)A,(select @rownum:=0)R";
        var sql2 = "SELECT location_name, count(*) AS Best_location FROM pal_note GROUP BY location_name ORDER BY Best_location DESC";
        connection.query(sql2, function (err, result) {
            if (err) console.error("err : " + err);
           Best_location = result;
        });
        connection.query(sql, function (err, rows) {
        	
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
            res.render('admin/statistics/statisticsNote', {rows:rows,user:req.session.user,Best_location:Best_location});
        });
        
        
    });
};



var statisticsAdvertiser = function(req, res){
	var pool = req.app.get('database');
	var ad;
	var event;
	var out;
	var top;
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
			return;
		}
	var adrevenuesql = 'SELECT sum(view_cnt) sum,DATE_FORMAT(created_date, "%Y") date FROM pal_ad where  date(created_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(created_date) <= date(now()) group by DATE_FORMAT(created_date, "%Y") order by date;';
	var eventnenuesql = 'SELECT sum(view_cnt) sum,DATE_FORMAT(a.created_date, "%Y") date, ad_type event FROM pal_ad a,pal_ad_event e where a.created_date=e.created_date and date(a.created_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(a.created_date) <= date(now()) group by DATE_FORMAT(a.created_date, "%Y") order by date;';
	var	outrevenuesql= 'SELECT sum(view_cnt) sum,DATE_FORMAT(a.created_date, "%Y") date, ad_type event FROM pal_ad a,pal_ad_outdoors o where a.created_date=o.created_date and date(a.created_date) >= date(subdate(now(), INTERVAL 6 YEAR)) and date(a.created_date) <= date(now()) group by DATE_FORMAT(a.created_date, "%Y") order by date;';
	var	topsql= 'SELECT sum(view_cnt) sum,id FROM pal_ad group by id order by sum desc limit 0 , 5;';
	
	conn.query(adrevenuesql, function(err, rows) {
		conn.release();
		ad = rows;
		});
	conn.query(eventnenuesql, function(err, rows) {
		event = rows;
		});
	conn.query(outrevenuesql, function(err, rows) {
		out = rows;
		});
	conn.query(topsql, function(err, rows) {
		top = rows;
		res.render("admin/statistics/statisticsAdvertiser",{ad:ad,event:event,out:out,top:top,user:req.session.user});
		});
	});
	
};

var customerFacing = function(req, res){
	res.render("admin/administration/customerFacing",{user:req.session.user});
};


var findMember = function(req, res) {
	
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
   	// 보여줄 페이지  sql문
        var List2 = "SELECT * FROM pal_user ;";
        var List3 = "select * from pal_advertiser;";
     // 저장되어있는 글개수 sql문
      
        connection.query(List3, function (err, rows2) {
        	if(err) console.error("err : " + err);
        	console.log("rows2 : " + JSON.stringify(rows2));
        	var abc = rows2;
        
        connection.query(List2, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
            res.render('admin/information/findMember', {title: '유저 조회',rows:rows,abc:abc,user:req.session.user});
        });
        });
        
    });
};
var findMessage = function(req, res) {
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
    	if (err) {
			connection.release(); // 반드시 해제해야 한답니다...이유는 몰라여
			return;
		}
            var  sqlAll = "SELECT id, DATE_FORMAT(created_date, '%Y-%m-%d_%H:%i:%s') created_date,location_name,lat,lng,view_cnt,title,content,media,likes,grade,private from pal_note";
            connection.query(sqlAll, function (err,rows) {
            if (err) console.error("err : " + err);
            console.log("result : " + JSON.stringify(rows));
            res.render('admin/information/findmessage',{rows:rows,user:req.session.user});
        });
    });
};

var mailing = function(req, res) {
	
	
	res.render('admin/service/mailing',{user:req.session.user});
		
};
var notice = function(req, res) {
	
	
	res.render('admin/service/notice',{user:req.session.user});
		
};
var infoUser = function(req,res){
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
   	// 보여줄 페이지  sql문
        var List = "SELECT * FROM pal_user ;";
     // 저장되어있는 글개수 sql문
      
        
        connection.query(List, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
            res.render('admin/information/findMember', {title: '유저 조회',rows:rows});
        });
        
        
    });
};
var gradeAdvertiser = function(req, res) {
	
	
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
   	// 보여줄 페이지  sql문
    	var request;
        var List = "SELECT * ,DATE_FORMAT(join_date, '%Y-%m-%d  %H:%i:%s') date  FROM pal_advertiser where grade = 'R'";
        var sql = "select * from pal_ad_request";
     // 저장되어있는 글개수 sql문
        connection.query(sql, function (err, result) {
        	if (err) console.error("err : " + err);
        	console.log("request : " + JSON.stringify(result));
        	request = result;
        });
      connection.query(List, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
            res.render('admin/information/gradeAdvertiser', {title: '사업자 등급 조회',rows:rows,user:req.session.user,request:request});
        });
        
		
    });
};

var updateAdvertiser = function(req, res) {
	var id = req.body.id;
console.log(typeof id);
		var pool = req.app.get('database');
	    pool.getConnection(function (err, connection) {
   	// 보여줄 페이지  sql문
    	var List = '';
    	if(typeof id=='object'){
        for(var a=0;a<id.length;a++){
       if(a==0){
        List += "update pal_advertiser set grade='A' where id in(";
       } 
       List += "'"+id[a]+"'";
       if(a!=id.length-1){
    	   List+=",";
       }
       if(a==id.length-1){
    	   List+=");";
       }
       }
    	}
    	if(typeof id=='string'){
    	List="update pal_advertiser set grade='A' where id = '"+id+"'";
    	}
        console.log(List);
        // F 에서 D 로 바꾸는 쿼리 문
    
     // 저장되어있는 글개수 sql문
      
        
        connection.query(List, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
           res.redirect('/gradeAdvertiser');
        });
        
		
    });
};

var updateAdvertiser2 = function(req, res) {
	var pool = req.app.get('database');
	
	var	id =req.params.id; 
    pool.getConnection(function (err, connection) {
    	    var List = "update pal_advertiser set grade='A' where id = '"+id+"'";
    	    connection.query(List, function (err, rows) {
                if (err) console.error("err : " + err);
               
                connection.release();
               res.redirect('/gradeAdvertiser');
            });
    });
};
var datepicker = function(req, res) {
	   var pool = req.app.get('database');
	    pool.getConnection(function (err, connection) {
	       var firstDate = req.body.datepicker;
	       var lastDate = req.body.datepicker2;
	       console.log("firstDate : "+firstDate);
	       console.log("lastDate : "+lastDate);
	       var sqlAll;
	       if(firstDate=='' && lastDate != ''){
	           sqlAll = "SELECT id, DATE_FORMAT(created_date, '%Y-%m-%d_%H:%i:%s') created_date,location_name,lat,lng,view_cnt,title,content,media,likes,grade,private from pal_note where created_date <= '"+lastDate+"'";
	        }
	        else if(lastDate=='' && firstDate != ''){
	            sqlAll = "SELECT id, DATE_FORMAT(created_date, '%Y-%m-%d_%H:%i:%s') created_date,location_name,lat,lng,view_cnt,title,content,media,likes,grade,private from pal_note  where created_date >= '"+firstDate+"'";
	        }else if(firstDate=='' && lastDate == ''){
	              sqlAll = "SELECT id, DATE_FORMAT(created_date, '%Y-%m-%d_%H:%i:%s') created_date,location_name,lat,lng,view_cnt,title,content,media,likes,grade,private from pal_note";
	        }else{
	        	lastDate = Number(lastDate)+1;
	           sqlAll = "SELECT id, DATE_FORMAT(created_date, '%Y-%m-%d_%H:%i:%s') created_date,location_name,lat,lng,view_cnt,title,content,media,likes,grade,private from pal_note  where created_date between '"+firstDate+"' and '"+lastDate+"'";
	        }
	       console.log(sqlAll);
	       connection.query(sqlAll, function (err,result) {
	            if (err) console.error("err : " + err);
	            console.log("result : " + JSON.stringify(result));
	            connection.release();
	            res.send(result);
	            res.end();
	        });
	    });
	};

var goCs = function(req, res) {
		
		
		res.render('admin/service/cs',{user:req.session.user});
			
};
	
var cs = function(req, res) {
	
	
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
   	// 보여줄 페이지  sql문
        var List = "SELECT * ,DATE_FORMAT(date, '%Y-%m-%d  %H:%i:%s') date  FROM pal_cs where complete = 'n'";
        var List2 = "SELECT * ,DATE_FORMAT(date, '%Y-%m-%d  %H:%i:%s') date  FROM pal_qna where complete = 'n'";
        var sql = "SELECT * ,DATE_FORMAT(date, '%Y-%m-%d  %H:%i:%s') date  FROM pal_qna where complete = 'n' ";
        var request;
     // 저장되어있는 글개수 sql문
        connection.query(sql, function (err, result) {
        	if (err) console.error("err : " + err);
        	console.log("request : " + JSON.stringify(result));
        	request = result;
        });
        
        
        connection.query(List2, function (err, rows2) {
        	if(err) console.error("err : " + err);
        	console.log("rows2 : " + JSON.stringify(rows2));
        	var adv = rows2;
        	
        
        connection.query(List, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
            res.render('admin/service/cs', {title: '사용자별 조회',rows:rows, adv:adv, user:req.session.user, request:request});
        });
        
        });
        
		
    });
};
var updateCs = function(req, res) {
	var id = req.body.id;
console.log(typeof id);
		var pool = req.app.get('database');
	    pool.getConnection(function (err, connection) {
   	// 보여줄 페이지  sql문
    	var List = '';
    	if(typeof id=='object'){
        for(var a=0;a<id.length;a++){
       if(a==0){
        List += "update pal_cs set complete='N' where id in(";
       } 
       List += "'"+id[a]+"'";
       if(a!=id.length-1){
    	   List+=",";
       }
       if(a==id.length-1){
    	   List+=");";
       }
       }
    	}
    	if(typeof id=='string'){
    	List="update pal_cs set grade='C' where id = '"+id+"'";
    	}
        console.log(List);
        // N 에서 C 로 바꾸는 쿼리 문
    
     // 저장되어있는 글개수 sql문
      
        
        connection.query(List, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
           res.redirect('/cs');
        });
        
		
    });
};
var updateCs2 = function(req, res) {
	var pool = req.app.get('database');
	
	var	id =req.params.id; 
    pool.getConnection(function (err, connection) {
    	 var List = "update pal_cs set complete='C' where id = '"+id+"'";
    	    connection.query(List, function (err, rows) {
                if (err) console.error("err : " + err);
               
                connection.release();
               res.redirect('/cs');
            });
    });
};

var sanction = function(req, res){
	var pool = req.app.get('database');
	var note;
	var ad;
	var userreport;
	var advertiserreport;
	var userprivate='';
	var advertiserprivate='';
	var repeat;
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); 
			return;
		}
	var userreportsql;
	var advertiserreportsql;
	var notereportsql="select id,DATE_FORMAT(created_date, '%Y-%m-%d  %H:%i:%s') created_date,title,content,report from pal_note where private != 3 order by report desc;";
	var adreportsql="select id,DATE_FORMAT(created_date, '%Y-%m-%d  %H:%i:%s') created_date,title,content,report from pal_ad where private != 3 order by report desc;";
	var userprivatesql="select id,private from pal_user where private = 3;";
	var advertiserprivatesql="select id,private from pal_advertiser where private = 3;";
	var repeatsql="select id,title,content,count(content) count,sum(report) report from pal_note where private != 3 group by id ,title,content order by count(content) desc;";
	conn.query(notereportsql, function(err, rows) {
		conn.release();
		note = rows;
		conn.query(adreportsql, function(err, rows) {
			ad = rows;
			conn.query(userprivatesql, function(err, rows) {
				console.log('a');
				if(rows.length>1){
				for(var a=0;a<rows.length;a++){
					userprivate +="'"+rows[a].id+"'";
					if(a!=rows.length-1){
						userprivate+=",";
					}
					}
				userreportsql="select id,title,content,sum(report) report from pal_note where id not in ("+userprivate+") group by id order by report desc;";
				}else if(rows.length==1){
				userreportsql="select id,title,content,sum(report) report from pal_note where id !='"+rows[0].id+"' group by id order by report desc;";
				}else{
				userreportsql="select id,title,content,sum(report) report from pal_note group by id order by report desc;";	
				}
				
				conn.query(userreportsql, function(err, rows) {
					console.log(userreportsql);
					userreport = rows;
					 console.log("rows : " + JSON.stringify(rows));
					 conn.query(advertiserprivatesql, function(err, rows) {
							if(rows.length>1){
								for(var a=0;a<rows.length;a++){
									advertiserprivate +="'"+rows[a].id+"'";
									if(a!=rows.length-1){
										advertiserprivate+=",";
										}
									}
								advertiserreportsql="select id,title,content,sum(report) report from pal_ad where id not in ("+advertiserprivate+") group by id order by report desc;";
								}else if(rows.length==1){
								advertiserreportsql="select id,title,content,sum(report) report from pal_ad where id !='"+rows[0].id+"' group by id order by report desc;";
								}else{
									advertiserreportsql="select id,title,content,sum(report) report from pal_ad group by id order by report desc;";	
									
								}
							conn.query(advertiserreportsql, function(err, rows) {
								console.log(advertiserreportsql);
								advertiser = rows;
								console.log('aaaa');
								conn.query(repeatsql, function(err, rows) {
									repeat = rows;
									res.render("admin/administration/sanction",{note:note,ad:ad,userreport:userreport,advertiser:advertiser,repeat:repeat,user:req.session.user});
									});
								});
							
							});
					});
				});
			});
		
		});

	
	
	
	
	

	});
};
var sanctionexception = function(req, res){
	var pool = req.app.get('database');
	var note;
	var ad;
	var userreport;
	var advertiserreport;
	var userprivate='';
	var advertiserprivate='';
	var repeat;
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); 
			return;
		}
	var userreportsql;
	var advertiserreportsql;
	var notereportsql="select id,DATE_FORMAT(created_date, '%Y-%m-%d  %H:%i:%s') created_date,title,content,report from pal_note where private = 3 order by report desc;";
	var adreportsql="select id,DATE_FORMAT(created_date, '%Y-%m-%d  %H:%i:%s') created_date,title,content,report from pal_ad where private = 3 order by report desc;";
	var userprivatesql="select id,private from pal_user where private = 3;";
	var advertiserprivatesql="select id,private from pal_advertiser where private = 3;";
	var repeatsql="select id,title,content,count(content) count,sum(report) report from pal_note where private = 3 group by id ,title,content order by count(content) desc;";
	conn.query(notereportsql, function(err, rows) {
		conn.release();
		note = rows;
		conn.query(adreportsql, function(err, rows) {
			ad = rows;
			conn.query(userprivatesql, function(err, rows) {
				console.log('a');
				if(rows.length>1){
				for(var a=0;a<rows.length;a++){
					userprivate +="'"+rows[a].id+"'";
					if(a!=rows.length-1){
						userprivate+=",";
					}
					}
				userreportsql="select id,title,content,sum(report) report from pal_note where id in ("+userprivate+") group by id order by report desc;";
				}else if(rows.length==1){
				userreportsql="select id,title,content,sum(report) report from pal_note where id ='"+rows[0].id+"' group by id order by report desc;";
				}else{
				userreportsql="select id,title,content,sum(report) report from pal_note where private = 3 group by id order by report desc;";	
				}
				
				conn.query(userreportsql, function(err, rows) {
					console.log(userreportsql);
					userreport = rows;
					 console.log("rows : " + JSON.stringify(rows));
					 conn.query(advertiserprivatesql, function(err, rows) {
							if(rows.length>1){
								for(var a=0;a<rows.length;a++){
									advertiserprivate +="'"+rows[a].id+"'";
									if(a!=rows.length-1){
										advertiserprivate+=",";
										}
									}
								advertiserreportsql="select id,title,content,sum(report) report from pal_ad where id in ("+advertiserprivate+") group by id order by report desc;";
								}else if(rows.length==1){
								advertiserreportsql="select id,title,content,sum(report) report from pal_ad where id ='"+rows[0].id+"' group by id order by report desc;";
								}else{
									advertiserreportsql="select id,title,content,sum(report) report from pal_ad where private = 3 group by id order by report desc;";	
									
								}
							conn.query(advertiserreportsql, function(err, rows) {
								console.log(advertiserreportsql);
								advertiser = rows;
								console.log('aaaa');
								conn.query(repeatsql, function(err, rows) {
									repeat = rows;
									res.render("admin/administration/sanctionexception",{note:note,ad:ad,userreport:userreport,advertiser:advertiser,repeat:repeat,user:req.session.user});
									});
								});
							
							});
					});
				});
			});
		
		});

	});
};
	

var noteexception = function(req,res){
	var pool = req.app.get('database');
	var id=req.params.id;
	var created_date=req.params.created_date;
    pool.getConnection(function (err, connection) {
    	
    	var noteexception = "update pal_note set private=3 where id= '"+id+"' and created_date = '"+created_date+"'";
        
        
        connection.query(noteexception, function (err, rows) {
            if (err) console.error("err : " + err);
            res.redirect('/sanction');
        });
            
        });
 
};
var adexception = function(req,res){
	var pool = req.app.get('database');
	var id=req.params.id;
	var created_date=req.params.created_date;
    pool.getConnection(function (err, connection) {
    	
    	var adexception = "update pal_ad set private=3 where id= '"+id+"' and created_date = '"+created_date+"'";
        
        
        connection.query(adexception, function (err, rows) {
            if (err) console.error("err : " + err);
            res.redirect('/sanction');
        });
            
        });
 
};
var userexception = function(req,res){
	var pool = req.app.get('database');
	var id=req.params.id;
    pool.getConnection(function (err, connection) {
    	
    	var userexception = "update pal_user set private=3 where id= '"+id+"'";
        console.log(userexception);
        
        connection.query(userexception, function (err, rows) {
            if (err) console.error("err : " + err);
            res.redirect('/sanction');
        });
            
        });
 
};
var advertiserexception = function(req,res){
	var pool = req.app.get('database');
	var id=req.params.id;
    pool.getConnection(function (err, connection) {
    	
    	var advertiserexception = "update pal_advertiser set private=3 where id= '"+id+"'";
        console.log(userexception);
        
        connection.query(advertiserexception, function (err, rows) {
            if (err) console.error("err : " + err);
            res.redirect('/sanction');
        });
            
        });
 
};
var repeatexception = function(req,res){
	var pool = req.app.get('database');
	var id=req.params.id;
	var content=req.params.content;
	var title=req.params.title;
    pool.getConnection(function (err, connection) {
    	
    	var repeatexception = "update pal_note set private=3 where id= '"+id+"' and content = '"+content+"' and title = '"+title+"'";
        console.log(repeatexception);
        
        connection.query(repeatexception, function (err, rows) {
            if (err) console.error("err : " + err);
            res.redirect('/sanction');
        });
            
        });
 
};

var goFaq = function(req, res) {
	res.render('admin/administration/writeFaq',{user:req.session.user});
};

var writeFaq = function(req, res) {
	var pool = req.app.get('database');
	pool.getConnection(function(err, conn) {
		if (err) {
			conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
			return;
		}

		var title = req.body.title;
		var content = req.body.content;

		var data = {
			title : title,
			content : content
			
		};
		var que = 'insert into pal_faq set ?';
		var exec = conn.query(que, data, function(err, result) {
			conn.release();
			console.log('실행 대상 SQL : ' + exec.sql);
			if (err) {
				console.log('SQL 실행 시 에러 발생함.');
				console.dir(err);
				
				
			}
			
			res.redirect("/adminPage");
		
		});
	});

};

module.exports.adminPage = adminPage;
module.exports.statisticsAll = statisticsAll;
module.exports.statisticsNote = statisticsNote;
module.exports.statisticsAdvertiser = statisticsAdvertiser;
module.exports.customerFacing = customerFacing;
module.exports.sanction = sanction;
module.exports.findMember = findMember;
module.exports.findMessage = findMessage;
module.exports.mailing = mailing;
module.exports.notice = notice;
module.exports.datepicker = datepicker;

module.exports.infoUser = infoUser;
module.exports.gradeAdvertiser = gradeAdvertiser;
module.exports.updateAdvertiser = updateAdvertiser;
module.exports.updateAdvertiser2 = updateAdvertiser2;

module.exports.goCs = goCs;
module.exports.updateCs = updateCs;
module.exports.updateCs2 = updateCs2;
module.exports.cs = cs;


module.exports.noteexception=noteexception;
module.exports.adexception=adexception;
module.exports.userexception=userexception;
module.exports.advertiserexception=advertiserexception;
module.exports.repeatexception=repeatexception;
module.exports.sanctionexception = sanctionexception;
module.exports.goFaq = goFaq;
module.exports.writeFaq = writeFaq;

