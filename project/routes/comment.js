
var list = function(req,res){
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
    	var id= req.params.id;
    	var date= req.params.date;
  	// 보여줄 페이지  sql문
        var sqlForSelectList = "SELECT to_id,from_id,content ,DATE_FORMAT(date, '%Y-%m-%d  %h:%i:%s') date,DATE_FORMAT(created_date, '%Y-%m-%d  %h:%i:%s') created_date FROM pal_comment where to_id= '"+id+"' and created_date = '"+date+"' ;";
        connection.query(sqlForSelectList, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            connection.release();
            res.render('note/comment', {title: ' 게시판 전체 글 조회', rows:rows,id:id,date:date});
        });
        
        
    });
};

// 글쓰기를 눌렀을시 html로 바로 보내줌
var insertForm = function(req,res){
	var id=req.params.id;
	var date=req.params.date;
	console.log(id ,date);
    res.render("note/commentForm",{id:id,date:date});
};

// 글작성후 전송을 눌렀을때
var insert = function(req,res){
	var pool = req.app.get('database');
	pool.getConnection(function(err, conn) {
        if (err) {
        	conn.release();  // 반드시 해제해야 합니다.
          return;
        }   

    	var to_id=req.body.to_id;
    	var created_date = req.body.created_date;
    	var content=req.body.content;
    	var from_id=req.body.from_id;

    	// 데이터를 객체로 만듭니다.
    	var data = {to_id:to_id,created_date:created_date, content:content, from_id:from_id};
    	var a= 'insert into pal_comment set ?';
        var exec = conn.query(a, data, function(err, result) {
        	conn.release(); 
        	console.log('실행 대상 SQL : ' + exec.sql);
        	if (err) {
        		console.log('SQL 실행 시 에러 발생함.');
        		console.dir(err);
        		return;
        	}
        	 res.redirect("/comment/list/"+to_id+","+created_date);
        });
	});
	
};


module.exports.insert = insert;
module.exports.insertForm = insertForm;
module.exports.list = list;

