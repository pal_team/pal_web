/**
 * Created by JiHoon on 2017. 1. 4..
 */

var success = {
    state:'success'
};
var error = [{
    state:'error'
}];


var getmynote = function (req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }

        var id = req.body.id;

        var columns = ['id'];
        var tablename = 'pal_note';

        var que = "select * from ?? where id=? ";
        var exec = conn.query(que, [tablename,id], function (err, rows) {
            conn.release();
            console.log('실행 대상 SQL : ' + exec.sql);

            if (rows.length > 0) {
                req.session.user = rows[0];


                console.log(req.session.user);
                console.dir(JSON.stringify(rows));

                // res.writeHead('200', {'Content-Type': 'application/json; charset = utf8'});
                res.write(JSON.stringify(rows));
                res.end();


            } else {
                res.end();
            }
        });
    });
};

var getfollow = function (req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }

        var id = req.body.id;

        var columns = ['id'];
        var tablename = 'pal_follow';

        var que = "select follow from ?? where id=? ";
        var exec = conn.query(que, [tablename,id], function (err, rows) {
            conn.release();
            console.log('실행 대상 SQL : ' + exec.sql);

            if (rows.length > 0) {
                req.session.user = rows[0];


                console.log(req.session.user);
                console.dir(JSON.stringify(rows));

                // res.writeHead('200', {'Content-Type': 'application/json; charset = utf8'});
                res.write(JSON.stringify(rows));
                res.end();


            } else {
                res.end();
            }
        });
    });
};

var getfollower = function (req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }

        var id = req.body.id;

        var columns = ['id'];
        var tablename = 'pal_follow';

        var que = "select id as 'follower' from ?? where follow=? ";
        var exec = conn.query(que, [tablename,id], function (err, rows) {
            conn.release();
            console.log('실행 대상 SQL : ' + exec.sql);

            if (rows.length > 0) {
                req.session.user = rows[0];


                console.log(req.session.user);
                console.dir(JSON.stringify(rows));

                // res.writeHead('200', {'Content-Type': 'application/json; charset = utf8'});
                res.write(JSON.stringify(rows));
                res.end();


            } else {
                res.end();
            }
        });
    });
};

var mauthUser = function (req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            return;
        }

        var id = req.body.id;
        var password = req.body.password;
        var token = req.body.tokeninfo;

        var columns = ['id', 'password'];
        var tablename = 'pal_user';

        var que = "select * from ?? where id=? and password =?";
        var exec = conn.query(que, [tablename,id,password], function (err, rows) {
            console.log('실행 대상 SQL : ' + exec.sql);

            if (rows.length > 0) {
                req.session.user = rows[0];
                console.log('로그인 완료!.');
                tokeninfo = {
                    token : token
                }
                que = 'update ?? set ? where id = ?';
                exec = conn.query(que, [tablename, tokeninfo,id] , function (err, result) {
                    conn.release();
                    console.log('실행 대상 SQL : ' + exec.sql);
                    if (err) {
                        console.log('SQL 실행 시 에러 발생함.');
                        console.dir(err);
                        return;
                    }
                });
                console.log(req.session.user);
                console.dir(JSON.stringify(rows));

                // res.writeHead('200', {'Content-Type': 'application/json; charset = utf8'});
                res.write(JSON.stringify(rows));
                res.end();
            } else {
                res.write(JSON.stringify(error));
                res.end();
            }
        });
    });
};


var searchNote = function (req, res) {
    console.log('searchNote Call');
    var pool = req.app.get('database');

    pool.getConnection(function (err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다…이유는 몰라여
            return;
        }

        var id = req.body.id;
        var lat = req.body.lat;
        var lng = req.body.lng;
        var password = req.body.password;

        var columns = ['id', 'password'];
        var tablename = 'pal_note';

        var minLat = parseFloat((parseFloat(lat) - 0.0002).toFixed(4));
        var maxLat = parseFloat((parseFloat(lat) + 0.0002).toFixed(4));
        var minLng = parseFloat((parseFloat(lng) - 0.0002).toFixed(4));
        var maxLng = parseFloat((parseFloat(lng) + 0.0002).toFixed(4));

        console.log(minLat);
        console.log(maxLat);
        console.log(minLng);
        console.log(maxLng);
        var que = 'select * from ?? where lat between ? and ? and lng between ? and ?';
        var exec = conn.query(que, [tablename, minLat, maxLat, minLng, maxLng], function (err, rows) {

//         var que = 'select * from ?? where lat between 37.4888000 and 38 and lng between 127.034000 and 128';
//         var exec = conn.query(que, [tablename], function(err, rows) {

            conn.release();
            console.log('실행 대상 SQL : ' + exec.sql);
            if (rows.length > 0) {
                console.log(rows);
                res.write(JSON.stringify(rows));
            } else {
                console.log("Error");
            }
            res.end();
        });
    });
};


var getGeocode = function(latlng){
    var request = require('sync-request');
    var address;
    var response = request('get','https://maps.googleapis.com/maps/api/geocode/json',{
        'qs': {
            'latlng': latlng,
            'language' : 'ko',
            'key' : 'AIzaSyB6ZWcWMlJwvQJwddSgZSbvSF2KXMp-vy4'
        }
    });

    if(response.statusCode === 200){
        var json = JSON.parse(response.getBody('utf8'));
        if(json.results[1] != null)
            address = json.results[1].formatted_address;
        else
            address = 'undefined';
        return address;
    }
    return "Convert Geocode Error";

}
var createNote = function(req, res) {

    var pool = req.app.get('database');
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            res.write(JSON.stringify(error));
            return;
        }

        var id = req.body.id;
        var latStr = req.body.lat;
        var lngStr = req.body.lng;
        var lat = parseFloat(latStr);
        var lng = parseFloat(lngStr);
        var latlng = latStr+","+lngStr;
        var address = getGeocode(latlng);

        var title = req.body.title;
        var content = req.body.content;

        var data = {
            id : id,
            lat : lat,
            lng : lng,
            location_name : address,
            title : title,
            content : content
        };

        var que = 'insert into pal_note set ?';
        var exec = conn.query(que, data, function(err, result) {
            conn.release();
            console.log('실행 대상 SQL : ' + exec.sql);
            if (err) {
                console.log('SQL 실행 시 에러 발생함.');
                res.write(JSON.stringify(error));
            }
            res.write(JSON.stringify(success));
            res.end();
        });
    });
};

var logout = function(req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            res.write(JSON.stringify(error));
            return;
        }

        var id = req.body.id;
        var data = {
            token : null
        };

        var tablename = 'pal_user';

        var que = 'update ?? set ? where id = ?';
        var exec = conn.query(que, [tablename,data, id] , function (err, result) {
            conn.release();
            console.log('실행 대상 SQL : ' + exec.sql);
            if (err) {
                console.log('SQL 실행 시 에러 발생함.');
                console.dir(err);
                return;
            }
            res.end();
        });
    });
};

var removefollow = function(req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            res.write(JSON.stringify(error));
            return;
        }

        var id = req.body.id;
        var follow = req.body.follow;

        var que = 'delete from pal_follow where id = ? and follow = ?';
        var exec = conn.query(que, [id, follow] , function (err, result) {
            conn.release();
            console.log('실행 대상 SQL : ' + exec.sql);
            if (err) {
                console.log('SQL 실행 시 에러 발생함.');
                console.dir(err);
                return;
            }
            res.end();
        });
    });
};

var addfollow = function(req, res) {
    var pool = req.app.get('database');
    pool.getConnection(function(err, conn) {
        if (err) {
            conn.release(); // 반드시 해제해야 한답니다...이유는 몰라여
            res.write(JSON.stringify(error));
            return;
        }

        var id = req.body.id;
        var follow = req.body.follow;

        var data = {
            id:id,
            follow:follow
        };

        var que = 'insert into pal_follow set ?';
        var exec = conn.query(que, [data] , function (err, result) {
            console.log('실행 대상 SQL : ' + exec.sql);
            if (err) {
                console.log('SQL 실행 시 에러 발생함.');
                return;
            }
            que = 'select token from pal_user where id= ?';
            exec = conn.query(que, follow,function(err, result){
                conn.release();
                console.log('실행 대상 SQL : '+ exec.sql);
                if(err){
                    console.log('SQL 실행 시 에러 발생함.');
                    return;
                }
                if(result != null){
                    console.log(result[0].token);
                    var request = require('request');

                    var data={
                        title:"hello",
                        body:"world"
                    }
                    var options = {
                        url: 'https://fcm.googleapis.com/fcm/send',
                        method:'POST',
                        headers: {
                            'content-Type':'application/json',
                            'authorization':'key=AIzaSyB6ZWcWMlJwvQJwddSgZSbvSF2KXMp-vy4'
                        },
                        json: {
                            'notification': {
                                title:'새로운 팔로우',
                                body:id+' 님이 팔로우 하셨습니다.'
                            },
                            'to': result[0].token
                        }
                    }
                    request(options, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            console.log(body);
                        }
                    });
                }

            });
            res.end();
        });
    });
};


module.exports.mauthUser = mauthUser;
module.exports.createNote = createNote;
module.exports.searchNote = searchNote;
module.exports.getfollow = getfollow;
module.exports.getfollower = getfollower;
module.exports.getmynote = getmynote;
module.exports.logout = logout;
module.exports.addfollow = addfollow;
module.exports.removefollow = removefollow;


